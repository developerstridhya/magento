<?php
/**
 * Customer attribute controller
 *
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_Attributemanager
 */

namespace Tridhyatech\Attributemanager\Controller\Adminhtml\Customer\Attribute;

use Magento\Framework\Controller\Result;
use Magento\Framework\View\Result\PageFactory;

abstract class Attribute extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Tridhyatech_Attributemanager::customer_attribute';

    /**
     * @var string
     */
    protected $_entityTypeId;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Eav\Model\Entity $eavEntity
     * @param \Magento\Catalog\Model\Product\Url $productUrl
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Eav\Model\Entity $eavEntity,
        \Magento\Catalog\Model\Product\Url $productUrl,
        PageFactory $resultPageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->resultPageFactory = $resultPageFactory;
        $this->eavEntity = $eavEntity;
        $this->productUrl = $productUrl;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $this->_entityTypeId = $this->eavEntity->setType(\Magento\Customer\Model\Customer::ENTITY)->getTypeId();

        return parent::dispatch($request);
    }

    /**
     * Creation action page request
     *
     * @param \Magento\Framework\Phrase|null $title
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function createActionPage($title = null)
    {
        $resultPage = $this->resultPageFactory->create();
        if ($this->getRequest()->getParam('popup')) {
            if ($this->getRequest()->getParam('product_tab') === 'variations') {
                $resultPage->addHandle(['popup', 'catalog_product_attribute_edit_product_tab_variations_popup']);
            } else {
                $resultPage->addHandle(['popup', 'catalog_product_attribute_edit_popup']);
            }
            $pageConfig = $resultPage->getConfig();
            $pageConfig->addBodyClass('attribute-popup');
        } else {
            $resultPage->addBreadcrumb(__('Catalog'), __('Catalog'))
                ->addBreadcrumb(__('Manage Customer Attributes'), __('Manage Customer Attributes'))
                ->setActiveMenu('Tridhyatech_Attributemanager::customer_attribute');
            if (!empty($title)) {
                $resultPage->addBreadcrumb($title, $title);
            }
        }
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Customer Attribute'));
        return $resultPage;
    }

    /**
     * Generate code from label
     *
     * @param string $label
     * @return string
     */
    protected function generateCode($label)
    {
        $code = substr(
            preg_replace(
                '/[^a-z_0-9]/',
                '_',
                $this->productUrl->formatUrlKey($label)
            ),
            0,
            30
        );
        $validatorAttrCode = new \Zend_Validate_Regex(['pattern' => '/^[a-z][a-z_0-9]{0,29}[a-z0-9]$/']);
        if (!$validatorAttrCode->isValid($code)) {
            $code = 'attr_' . ($code ?: substr(hash('sha256', time()), 0, 8));
        }
        return $code;
    }
}
