<?php
/**
 * Helper data class
 *
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_Attributemanager
 */

namespace Tridhyatech\Attributemanager\Helper;

use Magento\Customer\Model\ResourceModel\Address\Attribute\CollectionFactory;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Constructor
     *
     * @param Context $context
     * @param EntityFactory $customerEntityFactory
     * @param AttributeFactory $attributeFactory
     * @param ConfigFactory $eavAttributeFactory
     * @param CustomerFactory $customerFactory
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $collectionFactory
     * @param AddressFactory $addressModel
     * @param ProductMetadataInterface $productMetadata
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Eav\Model\EntityFactory $customerEntityFactory,
        \Magento\Customer\Model\AttributeFactory $attributeFactory,
        \Magento\Eav\Model\ConfigFactory $eavAttributeFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        \Magento\Customer\Model\AddressFactory $addressModel,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        parent::__construct($context);
        $this->_customerEntityFactory = $customerEntityFactory;
        $this->_attributeFactory = $attributeFactory;
        $this->_eavAttribute = $eavAttributeFactory;
        $this->_storeManager = $storeManager;
        $this->_customerFactory = $customerFactory;
        $this->collection = $collectionFactory->create();
        $this->addressModel = $addressModel;
        $this->_productMetadata = $productMetadata;
    }

    /**
     * Get magento version
     */
    public function getMagentoVersion()
    {
        return $this->_productMetadata->getVersion();
    }

    /**
     * Get attribute input type
     *
     * @param string!null $inputType
     */
    public function getAttributeInputType($inputType = null)
    {
        $inputTypes = [
            'multiselect' => [
                'backend_model' => \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend::class,
                'source_model' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class
            ],
            'boolean' => [
                'source_model' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class
            ]
        ];

        if ($inputType === null) {
            return $inputTypes;
        } else {
            if (isset($inputTypes[$inputType])) {
                return $inputTypes[$inputType];
            }
        }
        return [];
    }

    /**
     * Get attribute backend model by input type
     *
     * @param string $inputType
     */
    public function getAttributeBackendModelByInputType($inputType)
    {
        $inputTypes = $this->getAttributeInputType();
        
        if (!empty($inputTypes[$inputType]['backend_model'])) {
            return $inputTypes[$inputType]['backend_model'];
        }
        return null;
    }

    /**
     * Get attribute source model by input type
     *
     * @param string $inputType
     */
    public function getAttributeSourceModelByInputType($inputType)
    {
        $inputTypes = $this->getAttributeInputType();
        if (!empty($inputTypes[$inputType]['source_model'])) {
            return $inputTypes[$inputType]['source_model'];
        }
        return null;
    }

    /**
     * Get customer user defined attributes
     */
    public function getCustomerUserDefinedAttributes()
    {
        $entityTypeId = $this->_customerEntityFactory->create()
                ->setType(\Magento\Customer\Model\Customer::ENTITY)
                ->getTypeId();
        $attribute = $this->_attributeFactory->create()
                ->setEntityTypeId($entityTypeId);
        $collection = $attribute->getCollection()
                ->addVisibleFilter()
                ->addFieldToFilter('is_user_defined', 1)
                ->addFieldToFilter('is_visible', 1)
                ->setOrder('sort_order', 'ASC');
        return $collection;
    }

    /**
     * Is attribute for customer account create
     *
     * @param string $attributeCode
     */
    public function isAttributeForCustomerAccountCreate($attributeCode)
    {
        $attribute   = $this->_eavAttribute->create()
                ->getAttribute('customer', $attributeCode);
        $usedInForms = $attribute->getUsedInForms();
        
        if (in_array('customer_account_create', $usedInForms)) {
            return true;
        }
         return false;
    }

    /**
     * Is attribute for customer account edit
     *
     * @param string $attributeCode
     */
    public function isAttribureForCustomerAccountEdit($attributeCode)
    {
        $attribute   = $this->_eavAttribute->create()
                ->getAttribute('customer', $attributeCode);
        $usedInForms = $attribute->getUsedInForms();
        
        if (in_array('customer_account_edit', $usedInForms)) {
            return true;
        }
         return false;
    }

    /**
     * Get store id
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getStoreId();
    }

    /**
     * Get attribute options
     *
     * @param string $attributeCode
     */
    public function getAttributeOptions($attributeCode)
    {
        $customerEntity = \Magento\Customer\Model\Customer::ENTITY;
        $options = $this->_eavAttribute->create()->getAttribute($customerEntity, $attributeCode)
                ->getSource()->getAllOptions();
         return $options;
    }

    /**
     * Get customer address attribute options
     *
     * @param string $attributeCode
     */
    public function getCustomerAddressAttributeOptions($attributeCode)
    {
        $customerEntity = 'customer_address';
        $options = $this->_eavAttribute->create()->getAttribute($customerEntity, $attributeCode)
                ->getSource()->getAllOptions();
         return $options;
    }

    /**
     * Get customer object
     *
     * @param int $customerId
     */
    public function getCustomer($customerId)
    {
        $customer = $this->_customerFactory->create()->load($customerId);
        return $customer;
    }

    /**
     * Get customer address user defined attributes
     */
    public function getCustomerAddressUserDefinedAttributes()
    {
        $this->collection->addFieldToFilter('additional_table.is_visible', ['eq' => 1]);
        $this->collection->addFieldToFilter('main_table.is_user_defined', ['eq' => 1]);
        $collection = $this->collection;
        
        return $collection;
    }

    /**
     * Is attribute for customer address create edit
     *
     * @param string $attributeCode
     * @param int $addressId
     */
    public function isAttribureForCustomerAddressCreateEdit($attributeCode, $addressId)
    {
        $attribute   = $this->_eavAttribute->create()
                ->getAttribute('customer_address', $attributeCode);
        $usedInForms = $attribute->getUsedInForms();
        
        if ($addressId != '') {
            if (in_array('customer_address_edit', $usedInForms)) {
                return true;
            }
        } else {
            if (in_array('customer_address_new', $usedInForms)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get customer address attibute default value
     *
     * @param string $attributeCode
     */
    public function getCustomerAddressAttributeDefaultValue($attributeCode)
    {
        $attribute   = $this->_eavAttribute->create()->getAttribute('customer_address', $attributeCode);

        if ($attribute->getData('is_datetime_attribute') == 1) {
            $defaultValue = $attribute->getData('datetime_attribute_default_value');
        } else {
            $defaultValue = $attribute->getDefaultValue();
        }
        return $defaultValue;
    }

    /**
     * Get customer address default values
     *
     * @param int $addressId
     */
    public function getCustomerAddressDefaultValues($addressId)
    {
        $address = $this->addressModel->create();
        $address->load($addressId);
        return $address;
    }

    /**
     * Get customer attribute default value
     *
     * @param string $attributeCode
     */
    public function getCustomerAttributeDefaultValue($attributeCode)
    {
        $attribute   = $this->_eavAttribute->create()->getAttribute('customer', $attributeCode);

        if ($attribute->getData('is_datetime_attribute') == 1) {
            $defaultValue = $attribute->getData('datetime_attribute_default_value');
        } else {
            $defaultValue = $attribute->getDefaultValue();
        }
        return $defaultValue;
    }
}
