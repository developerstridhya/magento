# Attribute Manager By Tridhya Tech

## Description

Customer attributes provide the information that is required to support the order, fulfillment, and customer management processes. Because your business is unique, you might need fields in addition to those provided by the system. You can add custom attributes to the Account Information, Address Book, and Billing Information sections of the customer’s account. Customer address attributes can also be used in the Billing Information section during checkout, or when guests register for an account. Category Attributes provide additional information that is related to a specific category.

The Attribute Manager allows the admin to create an infinite number of category attributes, customer attributes, and customer address attributes from the admin menu itself. Also, the admin will have a selection of in which forms do they want to display such attributes. These attributes will reflect as additional fields in selected frontend forms.

## Key Features:

- Easy to use.
- Add an infinite number of attributes.
- Various attribute types for the customer, customer address, and category: textbox, text area, text editor, date, yes/no, dropdown, multi-select.
- Apply default value to the attributes.
- Apply different validations to the attributes.
- Ability to make attribute fields mandatory.
- See all the created attributes in the admin grid.
- Admin can edit and delete the attribute.
- Filter the attributes by the attribute’s name, code, etc.
- Compatible up to 2.3.x.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/attributemanager
        * php bin/magento module:enable Tridhyatech_Attributemanager
        * php bin/magento setup:upgrade
