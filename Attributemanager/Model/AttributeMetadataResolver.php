<?php
/**
 * AttributeMetadataResolver class for customer and customer address attributes
 *
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_Attributemanager
 */

declare(strict_types=1);

namespace Tridhyatech\Attributemanager\Model;

use Magento\Customer\Model\ResourceModel\Address\Attribute\Source\CountryWithWebsites;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Ui\DataProvider\EavValidationRules;
use Magento\Ui\Component\Form\Field;
use Magento\Eav\Model\Entity\Type;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Config\Share as ShareConfig;
use Magento\Customer\Model\FileUploaderDataResolver;
use Magento\Ui\Component\Form\Element\Wysiwyg as WysiwygElement;
use Magento\Customer\Api\Data\OptionInterfaceFactory;

class AttributeMetadataResolver extends \Magento\Customer\Model\AttributeMetadataResolver
{
    /**
     * EAV attribute properties to fetch from meta storage
     * @var array
     */
    private static $metaProperties = [
        'dataType' => 'frontend_input',
        'visible' => 'is_visible',
        'required' => 'is_required',
        'label' => 'frontend_label',
        'sortOrder' => 'sort_order',
        'notice' => 'note',
        'default' => 'default_value',
        'size' => 'multiline_count',
    ];

    /**
     * Form element mapping
     *
     * @var array
     */
    private static $formElement = [
        'text' => 'input',
        'hidden' => 'input',
        'boolean' => 'checkbox',
    ];

    /**
     * Constructor
     *
     * @param CountryWithWebsites $countryWithWebsiteSource
     * @param EavValidationRules $eavValidationRules
     * @param FileUploaderDataResolver $fileUploaderDataResolver
     * @param ContextInterface $context
     * @param ShareConfig $shareConfig
     * @param OptionInterfaceFactory $optionFactory
     * @param Collection $entityAttributeOptionCollection
     */
    public function __construct(
        CountryWithWebsites $countryWithWebsiteSource,
        EavValidationRules $eavValidationRules,
        FileUploaderDataResolver $fileUploaderDataResolver,
        ContextInterface $context,
        ShareConfig $shareConfig,
        OptionInterfaceFactory $optionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $entityAttributeOptionCollection
    ) {
        parent::__construct(
            $countryWithWebsiteSource,
            $eavValidationRules,
            $fileUploaderDataResolver,
            $context,
            $shareConfig
        );
        $this->countryWithWebsiteSource = $countryWithWebsiteSource;
        $this->eavValidationRules = $eavValidationRules;
        $this->fileUploaderDataResolver = $fileUploaderDataResolver;
        $this->context = $context;
        $this->shareConfig = $shareConfig;
        $this->optionFactory = $optionFactory;
        $this->_entityAttributeOptionCollection = $entityAttributeOptionCollection;
    }

    /**
     * Get meta data of the customer or customer address attribute
     *
     * @param AbstractAttribute $attribute
     * @param Type $entityType
     * @param bool $allowToShowHiddenAttributes
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAttributesMeta(
        AbstractAttribute $attribute,
        Type $entityType,
        bool $allowToShowHiddenAttributes
    ): array {
        $meta = $this->modifyBooleanAttributeMeta($attribute);

        // use getDataUsingMethod, since some getters are defined and apply additional processing of returning value
        foreach (self::$metaProperties as $metaName => $origName) {
            if ($origName == 'default_value' &&
                $attribute->getData('frontend_input') == 'date' &&
                $attribute->getData('is_datetime_attribute') == '1'
            ) {
                $defaultDatetime = $attribute->getData('datetime_attribute_default_value');
                if ($defaultDatetime != '' && $defaultDatetime != null) {
                    $value = date('Y-m-d H:i:s', strtotime($attribute->getData('datetime_attribute_default_value')));
                }
            } else {
                $value = $attribute->getDataUsingMethod($origName);
            }
            if ($metaName === 'label') {
                $meta['arguments']['data']['config'][$metaName] = __($value);
                $meta['arguments']['data']['config']['__disableTmpl'] = [$metaName => true];
            } else {
                $meta['arguments']['data']['config'][$metaName] = $value;
            }
            if ('frontend_input' === $origName) {
                $meta['arguments']['data']['config']['formElement'] = self::$formElement[$value] ?? $value;
            }
        }

        if ($attribute->usesSource()) {
            if ($attribute->getAttributeCode() === AddressInterface::COUNTRY_ID) {
                $meta['arguments']['data']['config']['options'] = $this->countryWithWebsiteSource
                    ->getAllOptions();
            } else {
                $options = $attribute->getSource()->getAllOptions();
                array_walk(
                    $options,
                    function (&$item) {
                        $item['__disableTmpl'] = ['label' => true];
                    }
                );
                $meta['arguments']['data']['config']['options'] = $options;
            }
        }

        $rules = $this->eavValidationRules->build($attribute, $meta['arguments']['data']['config']);
        if (!empty($rules)) {
            $meta['arguments']['data']['config']['validation'] = $rules;
        }

        $meta['arguments']['data']['config']['componentType'] = Field::NAME;
        $meta['arguments']['data']['config']['visible'] = $this->canShowAttribute(
            $attribute,
            $allowToShowHiddenAttributes
        );

        $this->fileUploaderDataResolver->overrideFileUploaderMetadata(
            $entityType,
            $attribute,
            $meta['arguments']['data']['config']
        );

        if ($attribute->getData('is_wysiwyg_enabled') == 1) {

            $meta['arguments']['data']['config']['formElement'] = WysiwygElement::NAME;
            $meta['arguments']['data']['config']['wysiwyg'] = true;
            $meta['arguments']['data']['config']['class'] = \Magento\Ui\Component\Form\Element\Wysiwyg::class;
            $meta['arguments']['data']['config']['rows'] = 8;
            $meta['arguments']['data']['config']['template'] = 'ui/form/field';
            $meta['arguments']['data']['config']['wysiwygConfigData']['height'] = '100px';
            $meta['arguments']['data']['config']['wysiwygConfigData']['add_variables'] = false;
            $meta['arguments']['data']['config']['wysiwygConfigData']['add_widgets'] = false;
            $meta['arguments']['data']['config']['wysiwygConfigData']['add_images'] = true;
            $meta['arguments']['data']['config']['wysiwygConfigData']['add_directives'] = true;
            $meta['arguments']['data']['config']['wysiwygConfigData']['is_pagebuilder_enabled'] = false;
        }

        //For validation rule
        $validationArr = [];
        if ($attribute->getData('is_required') == 1) {
            $validationArr['required-entry'] = '1';
        }

        if ($attribute->getData('frontend_class') != '') {
            $validationName = $attribute->getData('frontend_class');
            $validationArr[$validationName] = '1';
        }

        if (count($validationArr) > 0) {
            $meta['arguments']['data']['config']['validation'] = $validationArr;
        }

        if ($attribute->getData('frontend_input') == 'date' && $attribute->getData('is_datetime_attribute') == '1') {
            $meta['arguments']['data']['config']['formElement'] = 'date';
            $meta['arguments']['data']['config']['options'] = [
                'showsTime' => '1',
                'dateFormat' => 'M/d/yy',
                'timeFormat' => 'h:mm a'
            ];
            $meta['arguments']['data']['config']['storeTimeZone'] = 'storeTimeZone';
        }
        
        return $meta;
    }

    /**
     * Detect can we show attribute on specific form or not
     *
     * @param AbstractAttribute $customerAttribute
     * @param bool $allowToShowHiddenAttributes
     * @return bool
     */
    private function canShowAttribute(
        AbstractAttribute $customerAttribute,
        bool $allowToShowHiddenAttributes
    ) {
        return $allowToShowHiddenAttributes && (bool) $customerAttribute->getIsUserDefined()
            ? true
            : (bool) $customerAttribute->getIsVisible();
    }

    /**
     * Modify boolean attribute meta data
     *
     * @param AttributeInterface $attribute
     * @return array
     */
    private function modifyBooleanAttributeMeta(AttributeInterface $attribute): array
    {
        $meta = [];
        if ($attribute->getFrontendInput() === 'boolean') {
            $meta['arguments']['data']['config']['prefer'] = 'toggle';
            $meta['arguments']['data']['config']['valueMap'] = [
                'true' => '1',
                'false' => '0',
            ];
        }

        return $meta;
    }
}
