<?php
/**
 * Layout processor for checkout page
 *
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_Attributemanager
 */

namespace Tridhyatech\Attributemanager\Block\Checkout;

class LayoutProcessor
{
    /**
     * __construct function
     *
     * @param \Tridhyatech\Attributemanager\Helper\Data $helper
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Eav\Model\ConfigFactory $eavAttributeFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $optionCollection
     */
    public function __construct(
        \Tridhyatech\Attributemanager\Helper\Data $helper,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Eav\Model\ConfigFactory $eavAttributeFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $optionCollection
    ) {
        $this->helper = $helper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->customerSession = $customerSession;
        $this->_eavAttribute = $eavAttributeFactory;
        $this->optionCollection = $optionCollection;
    }

    /**
     * Plugin method afterProcess
     *
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     */
    public function afterProcess(\Magento\Checkout\Block\Checkout\LayoutProcessor $subject, array $jsLayout)
    {
        $customerAddressAttributesCollection = $this->helper->getCustomerAddressUserDefinedAttributes();
        
        if (count($customerAddressAttributesCollection) > 0) {
            
            $childrenAttributeArr = [];

            $attributeCount = 0;

            foreach ($customerAddressAttributesCollection as $attribute) {
                $attributeCode = $attribute->getAttributeCode();
                $attributeFromEav   = $this->_eavAttribute->create()
                        ->getAttribute('customer_address', $attributeCode);
                $usedInForms = $attributeFromEav->getUsedInForms();
                $customerAttr = null;
                if (in_array('customer_register_address', $usedInForms)) {
                    $attributeCount++;
                    $frontEndLabel = $attribute->getStoreLabel($this->helper->getStoreId());

                    if ($this->customerSession->isLoggedIn()) {
                        $customerId = $this->customerSession->getCustomer()->getId();
                        $customer = $this->customerRepositoryInterface->getById($customerId);

                        if (null !== $customer->getCustomAttribute($attributeCode)) {
                            $customerAttr = $customer->getCustomAttribute($attributeCode)->getValue();
                        } else {
                            $customerAttr = $this->helper->getCustomerAddressAttributeDefaultValue($attributeCode);
                        }
                    } else {
                        $customerAttr = $this->helper->getCustomerAddressAttributeDefaultValue($attributeCode);
                    }
                    
                    //For validation rule
                    $validationArr = [];
                    if ($attribute->getData('is_required') == 1) {
                        $validationArr['required-entry'] = '1';
                    }

                    if ($attribute->getData('frontend_class') != '') {
                        $validationName = $attribute->getData('frontend_class');
                        $validationArr[$validationName] = '1';
                    }

                    $fieldInput = $attribute->getFrontendInput();

                    $attributeId = $attribute->getAttributeId();

                    $sortOrder = $attribute->getSortOrder();
                    
                    $childrenAttributeArr[] = $this->addNewField(
                        $attribute,
                        $attributeCode,
                        $frontEndLabel,
                        $validationArr,
                        $fieldInput,
                        $customerAttr,
                        $sortOrder
                    );
                }
            }
            
            if ($attributeCount != 0) {
                $fieldsetGroup = [];

                $fieldsetGroup['component'] = 'Magento_Ui/js/form/components/group';
                $fieldsetGroup['label'] = __('Additional Information');
                $fieldsetGroup['dataScope'] = 'shippingAddress.additional-info';
                $fieldsetGroup['provider'] = 'checkoutProvider';
                $fieldsetGroup['sortOrder'] = '150';
                $fieldsetGroup['type'] = 'group';
                $fieldsetGroup['config'] = [
                    'template' => 'ui/group/group',
                    'additionalClasses' => 'additional-info'
                ];

                $fieldsetGroup['children'] = $childrenAttributeArr;
                
                $jsLayout['components']['checkout']
                         ['children']['steps']
                         ['children']['shipping-step']
                         ['children']['shippingAddress']
                         ['children']['shipping-address-fieldset']
                         ['children']['additional-info'] = $fieldsetGroup;
            }
        }
        
        return $jsLayout;
    }

    /**
     * Function addNewField
     *
     * @param string $attribute
     * @param string $attributeCode
     * @param string $frontEndLabel
     * @param array $validationArr
     * @param string $fieldInput
     * @param array $customerAttr
     * @param string $sortOrder
     */
    public function addNewField(
        $attribute,
        $attributeCode,
        $frontEndLabel,
        $validationArr,
        $fieldInput,
        $customerAttr,
        $sortOrder
    ) {
        $allOptions = [];
        $opt_val = [];
        $fieldAbstract = '';
        $fieldInputType = '';
        $class = '';

        if ($fieldInput == 'text') {
            $fieldInputType = 'input';
            $fieldAbstract =  'abstract';

            $class = '';
        } elseif ($fieldInput == 'date' && $attribute->getIsDatetimeAttribute() == '1') {
            $fieldInputType = 'date';
            $fieldAbstract =  'date';

            if ($customerAttr != null && $customerAttr != '') {
                $customerAttr = date('m/d/Y H:i:s', strtotime($customerAttr));
            }

            $allOptions = [
                'showsTime' => '1',
                'timeFormat' => 'HH:mm a'
            ];

        } elseif ($fieldInput == 'date') {
            $fieldInputType = 'date';
            $fieldAbstract =  'date';

            if ($customerAttr != null && $customerAttr != '') {
                $customerAttr = date('m/d/Y', strtotime($customerAttr));
            }

            $class = '';

        } elseif ($fieldInput == 'select') {
            $fieldInputType = 'select';
            $fieldAbstract =  'select';

            $class = '';

            if ($attribute->usesSource()) {
                $allOptions = $attribute->getSource()->getAllOptions();
            }

        } elseif ($fieldInput == 'boolean') {
            $fieldInputType = 'select';
            $fieldAbstract =  'select';
            $allOptions = [
                ['value' => '1', 'label' => 'Yes'],
                ['value' => '0', 'label' => 'No']
            ];

            $class = '';

        } elseif ($fieldInput == 'textarea') {
            $fieldInputType = 'textarea';
            $fieldAbstract =  'textarea';

            $class = '';

        } elseif ($fieldInput == 'multiselect') {
            $fieldInputType = 'multiselect';
            $fieldAbstract =  'multiselect';

            $class = '';

            if ($attribute->usesSource()) {
                $allOptions = $attribute->getSource()->getAllOptions();
            }

        }

        $customField = [];

        $customField = [
            'component' => 'Magento_Ui/js/form/element/'.$fieldAbstract,
            'config' => [
                'customScope' => 'shippingAddress',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/'.$fieldInputType,
            ],
            'dataScope' => $attributeCode,
            'label' => __($frontEndLabel),
            'provider' => 'checkoutProvider',
            'sortOrder' => $sortOrder,
            'validation' => $validationArr,
            'options' =>  $allOptions,
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'value' => $customerAttr
        ];

        return $customField;
    }
}
