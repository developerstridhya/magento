<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_Attributemanager
 */

namespace Tridhyatech\Attributemanager\Plugin\Eav\Attribute;

use Magento\Framework\Exception\LocalizedException;

class AbstractData
{
    /**
     * Validate data
     *
     * @param AbstractData $subject
     * @param array|string $value
     * @throws CoreException
     * @return bool
     */
    public function beforeValidateValue(\Magento\Eav\Model\Attribute\Data\AbstractData $subject, $value)
    {
        $attribute = $subject->getAttribute();

        foreach (['customer_register_address', 'customer_address_edit', 'checkout_index_index'] as $form) {
            if ($attribute->getIsUserDefined() && !in_array($form, $attribute->getUsedInForms(), true)) {
                $attribute->setIsRequired(false);
            }
        }

        return [$value];
    }
}
