<?php

/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_Slider
 */
namespace  Tridhyatech\Slider\Api\Data;

interface SliderInterface
{
    public const SLIDERID = 'slider_id';
    public const TITLE = 'title';
    public const SLIDERURL = 'sliderurl';
    public const ICON = 'icon';
    public const VIDEO = 'video';
    public const DISPLAYON = 'displayon';
    public const SORTORDER = 'sortorder';
    public const STATUS = 'status';
    public const CONTENT = 'content';
    public const RIGHTCONTENT = 'rightcontent';
    public const STOREVIEWS ="store_id";
    public const CMS = 'cms';
    public const PRODUCTS = 'products';

    /**
     * Get slider_id
     *
     * @return string|null
     */
    public function getSliderId();

    /**
     * Set slider_id
     *
     * @param string $sliderId
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setSliderId($sliderId);

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setTitle($title);

    /**
     * Get sliderurl
     *
     * @return string|null
     */
    public function getSliderurl();

    /**
     * Set sliderurl
     *
     * @param string $sliderurl
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setSliderurl($sliderurl);

    /**
     * Get icon
     *
     * @return string|null
     */
    public function getIcon();

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setIcon($icon);

    /**
     * Get video
     *
     * @return string|null
     */
    public function getVideo();

    /**
     * Set video
     *
     * @param string $video
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setVideo($video);

    /**
     * Get displayon
     *
     * @return string|null
     */
    public function getDisplayon();

    /**
     * Set displayon
     *
     * @param string $displayon
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setDisplayon($displayon);

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setStatus($status);

    /**
     * Get sortorder
     *
     * @return string|null
     */
    public function getSortorder();

    /**
     * Set sortorder
     *
     * @param string $sortorder
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setSortorder($sortorder);

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     *
     * @param string $content
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setContent($content);

    /**
     * Get rightcontent
     *
     * @return string|null
     */
    public function getRightcontent();

    /**
     * Set rightcontent
     *
     * @param string $rightcontent
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setRightcontent($rightcontent);

    /**
     * Get store_id
     *
     * @return string|null
     */
    public function getStoreviews();

    /**
     * Set store_id
     *
     * @param string $storeviews
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setStoreviews($storeviews);

    /**
     * Get cms
     *
     * @return string|null
     */
    public function getCms();

    /**
     * Set cms
     *
     * @param string $cms
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setCms($cms);

    /**
     * Get products
     *
     * @return string|null
     */
    public function getProducts();

    /**
     * Set products
     *
     * @param string $products
     *
     * @return \Tridhyatech\Slider\Api\Data\SliderInterface
     */
    public function setProducts($products);
}
