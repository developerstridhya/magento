# Dynamic Banner Slider By Tridhya Tech

## Description

Why is Dynamic Banner Slider important for the Magento store owner? Dynamic Banner Slider by Tridhyatech is a powerful marketing tool that helps store owners insert unlimited eye-catching banners on shopping sites to any page. The extension enhances visual effects and UX of visitors then increases CTR for your promotional campaigns. With Tridhyatech Dynamic Banner Slider extension, your website has the power to:

Attract customers right from the very first moments they are on the site.
Highlight your promotion, campaign or any events by using the banners.
Direct customer attention to special offers.
Ability to set banners on any of the pages of the store through “Display On” functionality. Like CMS Page, Product Page and Category Page without any code.
Easy to customize.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/slider
        * php bin/magento module:enable Tridhyatech_Slider
        * php bin/magento setup:upgrade
