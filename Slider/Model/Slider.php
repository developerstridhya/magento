<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_Slider
 */
namespace Tridhyatech\Slider\Model;

use Tridhyatech\Slider\Api\Data\SliderInterface;

class Slider extends \Magento\Framework\Model\AbstractModel implements SliderInterface
{
    /**
     * Initialize resource mode
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Tridhyatech\Slider\Model\ResourceModel\Grid::class);
    }

    /**
     * Get Slider Id
     *
     * @return array
     */
    public function getSliderId()
    {
        return $this->getData(self::SLIDERID);
    }

    /**
     * Set Slider Id
     *
     * @param string $sliderId
     *
     * @return array
     */
    public function setSliderId($sliderId)
    {
        return $this->setData(self::SLIDERID, $sliderId);
    }

    /**
     * Get Title
     *
     * @return array
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return array
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get Slider Url
     *
     * @return array
     */
    public function getSliderurl()
    {
        return $this->getData(self::SLIDERURL);
    }

    /**
     * Set Slider Url
     *
     * @param string $sliderurl
     *
     * @return array
     */
    public function setSliderurl($sliderurl)
    {
        return $this->setData(self::SLIDERURL, $sliderurl);
    }

    /**
     * Get Icons
     *
     * @return array
     */
    public function getIcon()
    {
        return $this->getData(self::ICON);
    }

    /**
     * Set Icons
     *
     * @param string $icon
     *
     * @return array
     */
    public function setIcon($icon)
    {
        return $this->setData(self::ICON, $icon);
    }

    /**
     * Get Video
     *
     * @return array
     */
    public function getVideo()
    {
        return $this->getData(self::VIDEO);
    }

    /**
     * Set Video
     *
     * @param string $video
     *
     * @return array
     */
    public function setVideo($video)
    {
        return $this->setData(self::VIDEO, $video);
    }

    /**
     * Display On
     *
     * @return array
     */
    public function getDisplayon()
    {
        return $this->getData(self::DISPLAYON);
    }

    /**
     * Display On
     *
     * @param string $displayon
     *
     * @return array
     */
    public function setDisplayon($displayon)
    {
        return $this->setData(self::DISPLAYON, $displayon);
    }

    /**
     * Get Status
     *
     * @return array
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set Status
     *
     * @param string $status
     *
     * @return array
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Sort Order
     *
     * @return array
     */
    public function getSortorder()
    {
        return $this->getData(self::SORTORDER);
    }

    /**
     * Set Sort Order
     *
     * @param string $sortorder
     *
     * @return array
     */
    public function setSortorder($sortorder)
    {
        return $this->setData(self::SORTORDER, $sortorder);
    }

    /**
     * Get Content
     *
     * @return array
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Set Content
     *
     * @param string $content
     *
     * @return array
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get Right Con
     *
     * @return array
     */
    public function getRightcontent()
    {
        return $this->getData(self::RIGHTCONTENT);
    }

    /**
     * Set Right Con
     *
     * @param string $rightcontent
     *
     * @return array
     */
    public function setRightcontent($rightcontent)
    {
        return $this->setData(self::RIGHTCONTENT, $rightcontent);
    }

    /**
     * Get Store View
     *
     * @return array
     */
    public function getStoreviews()
    {
        return $this->getData(self::STOREVIEWS);
    }

    /**
     * Set Store View
     *
     * @param string $storeviews
     *
     * @return array
     */
    public function setStoreviews($storeviews)
    {
        return $this->setData(self::STOREVIEWS, $storeviews);
    }

    /**
     * Get cms page
     *
     * @return array
     */
    public function getCms()
    {
        return $this->getData(self::CMS);
    }
    
    /**
     * Set CMS Page
     *
     * @param string $cms
     *
     * @return array
     */
    public function setCms($cms)
    {
        return $this->setData(self::CMS, $cms);
    }

    /**
     * Get Products
     *
     * @return array
     */
    public function getProducts()
    {
        return $this->getData(self::PRODUCTS);
    }
    
    /**
     * Set Product
     *
     * @param string $products
     *
     * @return array
     */
    public function setProducts($products)
    {
        return $this->setData(self::PRODUCTS, $products);
    }
}
