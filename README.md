# Tridhya Tech



## Who are we

We provide eCommerce App Development for your store and take your business worldwide. With your eCommerce app, extend your sales boundary and sell your products to every corner of the world. Our eCommerce App Developers will help you build your eCommerce Store by keeping every detail in mind.

Read more at - https://www.tridhyatech.com/overview

## Magento

Magento is an open-source e-commerce platform written in PHP. It uses multiple other PHP frameworks such as Laminas and Symfony. Magento source code is distributed under Open Software License v3.0.

We, Tridhya Tech provide development services in e-commerce platform - Magento and also implement our own Extensions which we offer to the developers via Tridhya Magento Store as well as Adobe Magento Marketplace.

## Links to check our in-house extensions

- Tridhya Magento Store - https://magento-store.tridhya.com/
- Adobe Magento Marketplace - https://marketplace.magento.com/partner/TridhyaTech

## Reach out to us

https://www.tridhyatech.com/contact
