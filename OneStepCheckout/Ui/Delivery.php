<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Ui;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Tridhyatech\OneStepCheckout\Helper\Data as TrDHelper;

class Delivery extends Column
{
    /**
     * @var TrDHelper
     */
    protected $_trdHelper;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param TrDHelper $trdHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        TrDHelper $trdHelper,
        array $components = [],
        array $data = []
    ) {
        $this->_trdHelper = $trdHelper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['delivery_time'])) {
                    $item[$this->getData('name')] = $this->_trdHelper->getDeliveryTimeLabel($item['delivery_time']);
                }
            }
        }
        return $dataSource;
    }
}
