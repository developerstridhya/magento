<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Plugin;

use Tridhyatech\OneStepCheckout\Helper\Data as TrDHelper;
use Tridhyatech\OneStepCheckout\Helper\Address;
use Closure;

class Validator
{
    /**
     * @var TrDHelper
     */
    protected $_trdHelper;

    /**
     * @var Address
     */
    protected $addressHelper;

    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $directoryData;

    /**
     * Validator constructor.
     *
     * @param TrDHelper $trdHelper
     * @param Address $addressHelper
     * @param \Magento\Directory\Helper\Data $directoryData
     */
    public function __construct(
        TrDHelper $trdHelper,
        Address $addressHelper,
        \Magento\Directory\Helper\Data $directoryData
    ) {
        $this->_trdHelper = $trdHelper;
        $this->addressHelper = $addressHelper;
        $this->_directoryData = $directoryData;
    }

    /**
     * Around Validate
     *
     * @param \Magento\Quote\Model\Quote\Address $subject
     * @param Closure $proceed
     *
     * @return bool|mixed
     */
    public function aroundValidate(\Magento\Quote\Model\Quote\Address $subject, Closure $proceed)
    {
        $result = $proceed();
        if (!$this->_trdHelper->isEnabled()) {
            return $result;
        }
        return $this->checkValidate($subject);
    }

    /**
     * Check Validate
     *
     * @param \Magento\Quote\Model\Quote\Address $subject
     *
     * @return bool|mixed
     */
    public function checkValidate($subject)
    {
        $errors = [];
        $enabledFields = $this->addressHelper->getAddressFields();
        if (in_array('firstname', $enabledFields)) {
            if (!\Zend_Validate::is($subject->getFirstname(), 'NotEmpty')) {
                $errors[] = __('Please enter the first name.');
            }
        }
        if (in_array('lastname', $enabledFields)) {
            if (!\Zend_Validate::is($subject->getLastname(), 'NotEmpty')) {
                $errors[] = __('Please enter the last name.');
            }
        }
        if (in_array('street', $enabledFields)) {
            if (!\Zend_Validate::is($subject->getStreetLine(1), 'NotEmpty')) {
                $errors[] = __('Please enter the street.');
            }
        }
        if (in_array('city', $enabledFields)) {
            if (!\Zend_Validate::is($subject->getCity(), 'NotEmpty')) {
                $errors[] = __('Please enter the city.');
            }
        }
        if (in_array('telephone', $enabledFields)) {
            if (!\Zend_Validate::is($subject->getTelephone(), 'NotEmpty')) {
                $errors[] = __('Please enter the phone number.');
            }
        }
        if (in_array('postcode', $enabledFields)) {
            $_havingOptionalZip = $this->_directoryData->getCountriesWithOptionalZip();
            if (!in_array(
                $subject->getCountryId(),
                $_havingOptionalZip
            ) && !\Zend_Validate::is(
                $subject->getPostcode(),
                'NotEmpty'
            )
            ) {
                $errors[] = __('Please enter the zip/postal code.');
            }
        }
        if (in_array('country_id', $enabledFields)) {
            if (!\Zend_Validate::is($subject->getCountryId(), 'NotEmpty')) {
                $errors[] = __('Please enter the country.');
            }
        }
        if (in_array('region_id', $enabledFields)) {
            if ($subject->getCountryModel()->getRegionCollection()->getSize() && !\Zend_Validate::is(
                $subject->getRegionId(),
                'NotEmpty'
            ) && $this->_directoryData->isRegionRequired(
                $subject->getCountryId()
            )
            ) {
                $errors[] = __('Please enter the state/province.');
            }
        }
        if (empty($errors) || $subject->getShouldIgnoreValidation()) {
            return true;
        }
        return $errors;
    }
}
