# One Step Checkout By Tridhya Tech

## Description

The One Step Checkout extension allows the store owner to supply their Magento 2 store with a user-friendly One Step Checkout to speed up the whole shopping process and heavily enhance customer satisfaction. This amazing extension makes the checkout process easier and quicker for customers than the default Magento 2 process. The customers can enhance their customer experience with a hassle-free checkout process.

## Key Features:

- Optimized Checkout page.
- Allows/Supports Gift Message.
- Allows/supports leaving order comments.
- Easy to install and customize.
- Single-button to complete/confirm the order.
- Smart single page to complete all the checkout steps.
- Allows customers to select delivery date and time and leaving delivery comments.
- Admin can add holiday and available days, time slot range, and intervals.
- Allows to change the sort order and enable/disable address fields.
- Allows to autofill address on the checkout page. Allows using automatic address suggestions from the specific country only.
- Show/Hide header or footer on the checkout page.
- Allows adding CMS static block with sort order in top and bottom of checkout page and at success page.
- Show/Hide order review, newsletter, discount code, order comment, terms and conditions, login link, gift messages on item/order, billing address, product thumbnail image, product list toggle on the checkout page.
- Admin can choose from 3 layout types (1 column or 2 columns or 3 columns).
- Admin can choose from 3 design types (default or custom or latest).
- Allows setting place order button color from the configuration.
- Allows setting default shipping method and payment method for checkout page.
- Allows setting guest checkout.
- Allows redirecting to the checkout page after adding to the cart.
- Allows setting description on the checkout page with support of HTML content.
- Allows setting route name and page title for checkout page.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/module-onestepcheckout
        * php bin/magento module:enable Tridhyatech_OneStepCheckout
        * php bin/magento setup:upgrade