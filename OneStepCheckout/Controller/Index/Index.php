<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Controller\Index;

use Exception;
use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Cart;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\Translate\InlineInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\LayoutFactory as ResultLayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\ShippingMethodManagementInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\TotalsCollector;
use Magento\Store\Model\StoreManagerInterface;
use Tridhyatech\OneStepCheckout\Helper\Data;
use Psr\Log\LoggerInterface;
use Magento\SalesRule\Model\Coupon;
use Tridhyatech\OneStepCheckout\Helper\Address;

class Index extends \Magento\Checkout\Controller\Onepage
{
    /**
     * @var Data
     */
    protected $_checkoutHelper;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var bool
     */
    protected $allowCollectTotals = false;

    /**
     * @var Configurable
     */
    protected $configurable;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var Coupon
     */
    protected $couponModel;

    /**
     * @var ShippingMethodManagementInterface
     */
    protected $shippingMethodManagement;

    /**
     * @var Address
     */
    protected $addressHelper;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param Registry $coreRegistry
     * @param InlineInterface $translateInline
     * @param Validator $formKeyValidator
     * @param ScopeConfigInterface $scopeConfig
     * @param LayoutFactory $layoutFactory
     * @param CartRepositoryInterface $quoteRepository
     * @param PageFactory $resultPageFactory
     * @param ResultLayoutFactory $resultLayoutFactory
     * @param RawFactory $resultRawFactory
     * @param JsonFactory $resultJsonFactory
     * @param ProductRepository $productRepository
     * @param StoreManagerInterface $storeManager
     * @param Cart $cart
     * @param LoggerInterface $logger
     * @param Configurable $configurable
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param Data $checkoutHelper
     * @param Coupon $couponModel
     * @param ShippingMethodManagementInterface $shippingMethodManagement
     * @param Address $addressHelper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        Registry $coreRegistry,
        InlineInterface $translateInline,
        Validator $formKeyValidator,
        ScopeConfigInterface $scopeConfig,
        LayoutFactory $layoutFactory,
        CartRepositoryInterface $quoteRepository,
        PageFactory $resultPageFactory,
        ResultLayoutFactory $resultLayoutFactory,
        RawFactory $resultRawFactory,
        JsonFactory $resultJsonFactory,
        ProductRepository $productRepository,
        StoreManagerInterface $storeManager,
        Cart $cart,
        LoggerInterface $logger,
        Configurable $configurable,
        \Magento\Checkout\Model\Session $checkoutSession,
        Data $checkoutHelper,
        Coupon $couponModel,
        ShippingMethodManagementInterface $shippingMethodManagement,
        Address $addressHelper
    ) {
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->cart = $cart;
        $this->logger = $logger;
        $this->configurable = $configurable;
        $this->checkoutSession = $checkoutSession;
        $this->_checkoutHelper = $checkoutHelper;
        $this->_couponModel = $couponModel;
        $this->shippingMethodManagement = $shippingMethodManagement;
        $this->addressHelper = $addressHelper;

        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement,
            $coreRegistry,
            $translateInline,
            $formKeyValidator,
            $scopeConfig,
            $layoutFactory,
            $quoteRepository,
            $resultPageFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $resultJsonFactory
        );
    }

    /**
     * Checks if current request uses SSL and referer also is secure.
     *
     * @return bool
     */
    private function isSecureRequest()
    {
        $request = $this->getRequest();
        $referrer = $request->getHeader('referer');
        $secure = false;

        if ($referrer) {
            $scheme = parse_url($referrer, PHP_URL_SCHEME);
            $secure = $scheme === 'https';
        }

        return $secure && $request->isSecure();
    }

    /**
     * Action
     *
     * @return ResponseInterface|Redirect|
      \Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     *
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function execute()
    {
        if (!$this->_checkoutHelper->isEnabled()) {
            $this->messageManager->addError(__('One step checkout is turned off.'));
            return $this->resultRedirectFactory->create()->setPath('checkout');
        }
        if (count($this->addressHelper->getAddressFields()) == 0) {
            $this->messageManager->addError(__('One step checkout is turned off 
                due to all address fields are disabled.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        $quote = $this->getOnepage()->getQuote();

        if (!$quote->hasItems() || $quote->getHasError() || !$quote->validateMinimumAmount()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        if (!$this->_customerSession->isLoggedIn() && !$this->_checkoutHelper->getAllowGuestCheckout($quote)) {
            $this->checkoutSession->setShowError(1);
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        // generate session ID only if connection is unsecure according to issues in session_regenerate_id function.
        // @see http://php.net/manual/en/function.session-regenerate-id.php
        if (!$this->isSecureRequest()) {
            $this->_customerSession->regenerateId();
        }
        $this->checkoutSession->setCartWasUpdated(false);
        $this->getOnepage()->initCheckout();

        $this->initDefaultMethods($quote);

        $resultPage = $this->resultPageFactory->create();
        $checkoutTitle = $this->_checkoutHelper->getCheckoutTitle();
        $resultPage->getConfig()->getTitle()->set($checkoutTitle);
        $resultPage->getConfig()->setPageLayout($this->_checkoutHelper->isShowHeaderFooter() ? '1column' : 'checkout');
        
        return $resultPage;
    }

    /**
     * Default shipping/payment method
     *
     * @param Quote $quote
     *
     * @return bool
     *
     * @throws Exception
     */
    public function initDefaultMethods(Quote $quote)
    {
        $shippingAddress = $quote->getShippingAddress();
        if (!$shippingAddress->getCountryId()) {
            $shippingAddress->setCountryId($this->_checkoutHelper->getDefaultCountryId())
                ->save();
        }

        try {
            $shippingAddress->setCollectShippingRates(true);

            /** @var TotalsCollector $totalsCollector */
            $totalsCollector = $this->_objectManager->get(TotalsCollector::class);
            $totalsCollector->collectAddressTotals($quote, $shippingAddress);

            $availableMethods = $this->shippingMethodManagement->getList($quote->getId());
            $method = null;
            if (count($availableMethods) == "1") {
                $method = array_shift($availableMethods);
            } else {
                if (!$shippingAddress->getShippingMethod() && count($availableMethods)) {
                    $defaultMethod = array_filter($availableMethods, [$this, 'filterMethod']);
                    if (count($defaultMethod)) {
                        $method = array_shift($defaultMethod);
                    }
                }
            }

            if ($method) {
                $methodCode = $method->getCarrierCode() . '_' . $method->getMethodCode();
                $this->getOnepage()->saveShippingMethod($methodCode);
            }
            $this->quoteRepository->save($quote);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Filter
     *
     * @param mixed $method
     *
     * @return bool
     */
    public function filterMethod($method)
    {
        $defaultShippingMethod = $this->_checkoutHelper->getDefaultShippingMethod();
        $methodCode = $method->getCarrierCode() . '_' . $method->getMethodCode();
        if ($methodCode === $defaultShippingMethod) {
            return true;
        }

        return false;
    }
}
