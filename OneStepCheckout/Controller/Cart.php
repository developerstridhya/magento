<?php
/**
 * DISCLAIMER
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Controller;

class Cart extends \Magento\Checkout\Controller\Cart\Index
{
    /**
     * Shopping cart display action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->oneHelper = $this->_objectManager
        ->get(\Tridhyatech\OneStepCheckout\Helper\Data::class);
        if ($this->_checkoutSession->getShowError()
            && !$this->cart->getCustomerSession()->isLoggedIn()
            && !$this->oneHelper->getAllowGuestCheckout(
                $this->_checkoutSession->getQuote()
            )) {
            $this->messageManager->addErrorMessage(__('Guest checkout is disabled.'));
            $this->_checkoutSession->setShowError(0);
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Shopping Cart'));
        return $resultPage;
    }
}
