<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Api;

/**
 * Interface CheckoutManagementInterface
 *
 * @api
 */
interface CheckoutManagementInterface
{
    /**
     * Update Item Quantity
     *
     * @param int $cartId
     * @param int $itemId
     * @param int $itemQty
     *
     * @return \Tridhyatech\OneStepCheckout\Api\Data\OneStepDetailsInterface
     */
    public function updateItemQty($cartId, $itemId, $itemQty);

    /**
     * Remove Item By Id
     *
     * @param int $cartId
     * @param int $itemId
     *
     * @return \Tridhyatech\OneStepCheckout\Api\Data\OneStepDetailsInterface
     */
    public function removeItemById($cartId, $itemId);

    /**
     * Retrive Payment Total Information
     *
     * @param int $cartId
     *
     * @return \Tridhyatech\OneStepCheckout\Api\Data\OneStepDetailsInterface
     */
    public function getPaymentTotalInformation($cartId);

    /**
     * Save Checkout Information
     *
     * @param int $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @param string[] $customerAttributes
     * @param string[] $additionInformation
     *
     * @return bool
     */
    public function saveCheckoutInformation(
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation,
        $customerAttributes = [],
        $additionInformation = []
    );
}
