<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class AddressSuggest implements ArrayInterface
{
    /**
     * Prepare Option Source
     *
     * @return array
     */
    public function getOptionsList()
    {
        return [
            ''       => __('No'),
            'google' => __('Google')
        ];
    }

   /**
    * Prepare Option Source
    *
    * @return array
    */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->getOptionsList() as $code => $label) {
            $options[] = [
                'value' => $code,
                'label' => $label
            ];
        }

        return $options;
    }
}
