<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Model;

use Exception;
use Tridhyatech\OneStepCheckout\Api\CheckoutManagementInterface;
use Tridhyatech\OneStepCheckout\Api\Data\OneStepDetailsInterface;
use Tridhyatech\OneStepCheckout\Helper\Data as TrDHelper;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Api\ShippingInformationManagementInterface;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\GiftMessage\Model\GiftMessageManager;
use Magento\GiftMessage\Model\Message;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CartTotalRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\PaymentMethodManagementInterface;
use Magento\Quote\Api\ShippingMethodManagementInterface;
use Magento\Quote\Model\Cart\ShippingMethodConverter;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\TotalsCollector;

class CheckoutManagement implements CheckoutManagementInterface
{
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var OscDetailFactory
     */
    protected $oneStepDetailsFactory;

    /**
     * @var ShippingMethodManagementInterface
     */
    protected $shippingMethodManagement;

    /**
     * @var PaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    /**
     * @var CartTotalRepositoryInterface
     */
    protected $cartTotalsRepository;

    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var ShippingInformationManagementInterface
     */
    protected $shippingInformationManagement;

    /**
     * @var TrDHelper
     */
    protected $trdHelper;

    /**
     * @var Message
     */
    protected $giftMessage;

    /**
     * @var GiftMessageManager
     */
    protected $giftMessageManagement;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var TotalsCollector
     */
    protected $_totalsCollector;

    /**
     * @var ShippingMethodConverter
     */
    protected $_shippingMethodConverter;

    /**
     * @var AddressInterface
     */
    protected $_addressInterface;

    /**
     * CheckoutManagement constructor.
     *
     * @param CartRepositoryInterface $cartRepository
     * @param OneStepDetailsFactory $oneStepDetailsFactory
     * @param ShippingMethodManagementInterface $shippingMethodManagement
     * @param PaymentMethodManagementInterface $paymentMethodManagement
     * @param CartTotalRepositoryInterface $cartTotalsRepository
     * @param UrlInterface $urlBuilder
     * @param Session $checkoutSession
     * @param ShippingInformationManagementInterface $shippingInformationManagement
     * @param TrDHelper $trdHelper
     * @param Message $giftMessage
     * @param GiftMessageManager $giftMessageManager
     * @param CustomerSession $customerSession
     * @param TotalsCollector $totalsCollector
     * @param ShippingMethodConverter $shippingMethodConverter
     * @param AddressInterface $addressInterface
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        OneStepDetailsFactory $oneStepDetailsFactory,
        ShippingMethodManagementInterface $shippingMethodManagement,
        PaymentMethodManagementInterface $paymentMethodManagement,
        CartTotalRepositoryInterface $cartTotalsRepository,
        UrlInterface $urlBuilder,
        Session $checkoutSession,
        ShippingInformationManagementInterface $shippingInformationManagement,
        TrDHelper $trdHelper,
        Message $giftMessage,
        GiftMessageManager $giftMessageManager,
        customerSession $customerSession,
        TotalsCollector $totalsCollector,
        ShippingMethodConverter $shippingMethodConverter,
        AddressInterface $addressInterface
    ) {
        $this->cartRepository = $cartRepository;
        $this->oneStepDetailsFactory = $oneStepDetailsFactory;
        $this->shippingMethodManagement = $shippingMethodManagement;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->cartTotalsRepository = $cartTotalsRepository;
        $this->_urlBuilder = $urlBuilder;
        $this->checkoutSession = $checkoutSession;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->trdHelper = $trdHelper;
        $this->giftMessage = $giftMessage;
        $this->giftMessageManagement = $giftMessageManager;
        $this->_customerSession = $customerSession;
        $this->_totalsCollector = $totalsCollector;
        $this->_shippingMethodConverter = $shippingMethodConverter;
        $this->_addressInterface = $addressInterface;
    }

    /**
     * @inheritDoc
     */
    public function removeItemById($cartId, $itemId)
    {
        /** @var Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);
        $quoteItem = $quote->getItemById($itemId);
        if (!$quoteItem) {
            throw new NoSuchEntityException(
                __('Cart %1 doesn\'t contain item  %2', $cartId, $itemId)
            );
        }
        try {
            $quote->removeItem($itemId);
            $this->cartRepository->save($quote);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Could not remove item from cart'));
        }

        return $this->getResponseData($quote);
    }

    /**
     * @inheritDoc
     */
    public function updateItemQty($cartId, $itemId, $itemQty)
    {
        if ($itemQty == 0) {
            return $this->removeItemById($cartId, $itemId);
        }

        /** @var Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);
        $quoteItem = $quote->getItemById($itemId);
        if (!$quoteItem) {
            throw new NoSuchEntityException(
                __('Cart %1 doesn\'t contain item  %2', $cartId, $itemId)
            );
        }

        try {
            $quoteItem->setQty($itemQty)->save();
            $this->cartRepository->save($quote);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Could not update item from cart'));
        }

        return $this->getResponseData($quote);
    }

    /**
     * @inheritDoc
     */
    public function getPaymentTotalInformation($cartId)
    {
        /** @var Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);

        return $this->getResponseData($quote);
    }

    /**
     * Response data to update onestepcheckout block
     *
     * @param Quote $quote
     *
     * @return OneStepDetailsInterface
     *
     * @throws NoSuchEntityException
     */
    public function getResponseData(Quote $quote)
    {
        /** @var OneStepDetailsInterface $oneStepDetails */
        $oneStepDetails = $this->oneStepDetailsFactory->create();

        if (!$quote->hasItems() || $quote->getHasError() || !$quote->validateMinimumAmount()) {
            $oneStepDetails->setRedirectUrl($this->_urlBuilder->getUrl('checkout/cart'));
        } else {
            if ($quote->getShippingAddress()->getCountryId()) {
                $oneStepDetails->setShippingMethods($this->getShippingMethods($quote));
            }
            $oneStepDetails->setPaymentMethods($this->paymentMethodManagement->getList($quote->getId()));
            $oneStepDetails->setTotals($this->cartTotalsRepository->get($quote->getId()));
        }

        return $oneStepDetails;
    }

    /**
     * Shipping Method
     *
     * @param Quote $quote
     *
     * @return array
     */
    public function getShippingMethods(Quote $quote)
    {
        $result = [];
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->addData($this->_addressInterface->getData());
        $shippingAddress->setCollectShippingRates(true);
        $this->_totalsCollector->collectAddressTotals($quote, $shippingAddress);
        $shippingRates = $shippingAddress->getGroupedAllShippingRates();
        foreach ($shippingRates as $carrierRates) {
            foreach ($carrierRates as $rate) {
                $result[] = $this->_shippingMethodConverter->modelToDataObject($rate, $quote->getQuoteCurrencyCode());
            }
        }

        return $result;
    }

    /**
     * Gift Message
     *
     * @param string $cartId
     * @param array $additionInformation
     *
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function addGiftMessage($cartId, $additionInformation)
    {
        /** @var Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);
        if ($this->trdHelper->isDisabledGiftMessage() && isset($additionInformation['giftMessage'])) {
            $giftMessage = TrDHelper::jsonDecode($additionInformation['giftMessage']);
            $this->giftMessage->setSender(isset($giftMessage['sender']) ? $giftMessage['sender'] : '');
            $this->giftMessage->setRecipient(isset($giftMessage['recipient']) ? $giftMessage['recipient'] : '');
            $this->giftMessage->setMessage(isset($giftMessage['message']) ? $giftMessage['message'] : '');
            $this->giftMessageManagement->setMessage($quote, 'quote', $this->giftMessage);
        }
    }

    /**
     * @inheritDoc
     */
    public function saveCheckoutInformation(
        $cartId,
        ShippingInformationInterface $addressInformation,
        $customerAttributes = [],
        $additionInformation = []
    ) {
        try {
            $additionInformation['customerAttributes'] = $customerAttributes;
            $this->checkoutSession->setOneStepData($additionInformation);
            $this->addGiftMessage($cartId, $additionInformation);
            if ($addressInformation->getShippingAddress()) {
                if ($this->_customerSession->isLoggedIn() && isset($additionInformation['billing-same-shipping'])
                    && !$additionInformation['billing-same-shipping']) {
                    $addressInformation->getShippingAddress()->setSaveInAddressBook(0);
                }
                $this->shippingInformationManagement->saveAddressInformation($cartId, $addressInformation);
            }
        } catch (Exception $e) {
            throw new InputException(__('Unable to save order information. Please check input data.'));
        }

        return true;
    }
}
