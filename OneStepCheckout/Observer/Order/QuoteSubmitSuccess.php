<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Observer\Order;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Model\Url;
use Magento\Downloadable\Model\Link;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject\Copy;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\CustomerManagement;
use Magento\Store\Model\ScopeInterface;
use downloadable_sales_copy_link;
use downloadable_sales_copy_order;
use downloadable_sales_copy_order_item;
use Exception;
use Magento\Customer\Api\GroupManagementInterface as CustomerGroupManagement;
use Magento\Downloadable\Model\Link\Purchased;
use Magento\Downloadable\Model\Link\Purchased\Item;
use Magento\Downloadable\Model\Link\Purchased\ItemFactory;
use Magento\Downloadable\Model\Link\PurchasedFactory;
use Magento\Downloadable\Model\Product\Type;
use Magento\Downloadable\Model\ResourceModel\Link\Purchased\Item\Collection;
use Magento\Downloadable\Model\ResourceModel\Link\Purchased\Item\CollectionFactory;

class QuoteSubmitSuccess implements ObserverInterface
{
    /**
     * @var AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var Url
     */
    protected $_customerUrl;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var SubscriberFactory
     */
    protected $subscriberFactory;

    /**
     * @var CustomerManagement
     */
    protected $customerManagement;

    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var PurchasedFactory
     */
    protected $_purchasedFactory;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ItemFactory
     */
    protected $_itemFactory;

    /**
     * @var Copy
     */
    protected $_objectCopyService;

    /**
     * @var CollectionFactory
     */
    protected $_itemsFactory;

    /**
     * @var CustomerGroupManagement
     */
    protected $customerGroupManagement;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * QuoteSubmitSuccess constructor.
     * @param AccountManagementInterface $accountManagement
     * @param Url $customerUrl
     * @param ManagerInterface $messageManager
     * @param CustomerSession $customerSession
     * @param SubscriberFactory $subscriberFactory
     * @param CustomerManagement $customerManagement
     * @param ScopeConfigInterface $scopeConfig
     * @param PurchasedFactory $purchasedFactory
     * @param ProductFactory $productFactory
     * @param ItemFactory $itemFactory
     * @param CollectionFactory $itemsFactory
     * @param Copy $objectCopyService
     * @param CustomerGroupManagement $customerGroupManagement
     * @param Session $checkoutSession
     */
    public function __construct(
        AccountManagementInterface $accountManagement,
        Url $customerUrl,
        ManagerInterface $messageManager,
        CustomerSession $customerSession,
        SubscriberFactory $subscriberFactory,
        CustomerManagement $customerManagement,
        ScopeConfigInterface $scopeConfig,
        PurchasedFactory $purchasedFactory,
        ProductFactory $productFactory,
        ItemFactory $itemFactory,
        CollectionFactory $itemsFactory,
        Copy $objectCopyService,
        CustomerGroupManagement $customerGroupManagement,
        Session $checkoutSession
    ) {
        $this->accountManagement = $accountManagement;
        $this->_customerUrl = $customerUrl;
        $this->messageManager = $messageManager;
        $this->_customerSession = $customerSession;
        $this->subscriberFactory = $subscriberFactory;
        $this->customerManagement = $customerManagement;
        $this->_scopeConfig = $scopeConfig;
        $this->_purchasedFactory = $purchasedFactory;
        $this->_productFactory = $productFactory;
        $this->_itemFactory = $itemFactory;
        $this->_itemsFactory = $itemsFactory;
        $this->_objectCopyService = $objectCopyService;
        $this->customerGroupManagement = $customerGroupManagement;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Save In Address Book
     *
     * @param Quote $quote
     */
    public function setSaveInAddressBook($quote)
    {
        $quote->getBillingAddress()->setSaveInAddressBook(1);
        $quote->getShippingAddress()->setSaveInAddressBook(1);
        $quote->save();
    }

    /**
     * Set the customer information to the order when choosing to create an account
     *
     * @param Quote $quote
     * @param Order $order
     * @return $this
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveCustomerInformation($quote, $order)
    {
        $billingAddress = $quote->getBillingAddress();
        $getFirstname = $billingAddress->getFirstname();
        $getLastname = $billingAddress->getLastname();
        $getMiddlename = $billingAddress->getMiddlename();
        $order->setCustomerFirstname(
            $getFirstname
        )->setCustomerLastname(
            $getLastname
        )->setCustomerMiddlename(
            $getMiddlename
        )->setCustomerGroupId(
            $this->customerGroupManagement->getDefaultGroup($quote->getStoreId())->getId()
        );

        return $this;
    }

    /**
     * Action Method
     *
     * @param Observer $observer
     *
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @type Quote $quote $quote */
        $quote = $observer->getEvent()->getQuote();
        $order = $observer->getEvent()->getOrder();

        $isDownloadableProduct = false;

        $onestepcheckoutData = $this->checkoutSession->getOneStepData();
        if (isset($onestepcheckoutData['register']) && $onestepcheckoutData['register']
            && isset($onestepcheckoutData['password'])
            && $onestepcheckoutData['password']
        ) {
            /* Save customer information for order and save address in address book */
            $this->saveCustomerInformation($quote, $order)->setSaveInAddressBook($quote);

            if ($this->checkoutSession->getIsCreatedAccountPaypalExpress()) {
                $customer = $quote->getCustomer();
                $this->checkoutSession->unsIsCreatedAccountPaypalExpress();
            } else {
                $customer = $this->customerManagement->create($order->getId());
            }
            $customerId = $customer->getId();
            /* Set customer Id for address */
            if ($customerId) {
                $quote->getBillingAddress()->setCustomerId($customerId);
                if ($shippingAddress = $quote->getShippingAddress()) {
                    $shippingAddress->setCustomerId($customerId);
                }
            }

            if ($customerId &&
                $this->accountManagement->getConfirmationStatus($customerId)
                === AccountManagement::ACCOUNT_CONFIRMATION_REQUIRED) {
                $url = $this->_customerUrl->getEmailConfirmationUrl($customer->getEmail());
                $this->messageManager->addSuccessMessage(
                    __(
                        'You must confirm your account. Please check your 
                        email for the confirmation link or 
                        <a href="%1">click here</a> for a new link.',
                        $url
                    )
                );
            } else {
                $this->_customerSession->loginById($customerId);
            }

            foreach ($quote->getAllItems() as $item) {
                if ($item->getProductType() == Type::TYPE_DOWNLOADABLE) {
                    $isDownloadableProduct = true;
                    break;
                }
            }
            if ($isDownloadableProduct) {
                foreach ($order->getAllItems() as $item) {
                    $this->setDownloadableOrderItem($item, $order);
                }
            }
        }

        if (isset($onestepcheckoutData['is_subscribed']) && $onestepcheckoutData['is_subscribed']) {
            if (!$this->_customerSession->isLoggedIn()) {
                $subscribedEmail = $quote->getBillingAddress()->getEmail();
            } else {
                $customer = $this->_customerSession->getCustomer();
                $subscribedEmail = $customer->getEmail();
            }

            try {
                $this->subscriberFactory->create()
                    ->subscribe($subscribedEmail);
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage(__('There is an error while subscribing for newsletter.'));
            }
        }
    }

    /**
     * Downloadable Order Item
     *
     * @param string $orderItem
     * @param mixed $order
     *
     * @return $this
     * @throws Exception
     */
    public function setDownloadableOrderItem($orderItem, $order)
    {
        if (!$orderItem->getId()) {
            return $this;
        }

        if ($orderItem->getProductType() != Type::TYPE_DOWNLOADABLE) {
            return $this;
        }

        $product = $orderItem->getProduct();
        $purchasedLink = $this->_purchasedFactory->create()->load($orderItem->getId(), 'order_item_id');
        if ($purchasedLink->getId()) {
            return $this;
        }
        
        if (!$product) {
            $product = $this->_productFactory->create()->setStoreId(
                $orderItem->getOrder()->getStoreId()
            )->load(
                $orderItem->getProductId()
            );
        }

        if ($product->getTypeId() == Type::TYPE_DOWNLOADABLE) {
            $links = $product->getTypeInstance()->getLinks($product);
            if ($linkIds = $orderItem->getProductOptionByCode('links')) {
                $buyLinkItem = $this->_purchasedFactory->create();
                $this->_objectCopyService->copyFieldsetToTarget(
                    downloadable_sales_copy_order::class,
                    'to_downloadable',
                    $orderItem->getOrder(),
                    $buyLinkItem
                );
                $this->_objectCopyService->copyFieldsetToTarget(
                    downloadable_sales_copy_order_item::class,
                    'to_downloadable',
                    $orderItem,
                    $buyLinkItem
                );
                $linkSectionTitle = $product->getLinksTitle() ? $product
                    ->getLinksTitle() : $this
                    ->_scopeConfig
                    ->getValue(
                        Link::XML_PATH_LINKS_TITLE,
                        ScopeInterface::SCOPE_STORE
                    );
                $buyLinkItem->setLinkSectionTitle($linkSectionTitle)->save();
                foreach ($linkIds as $linkId) {
                    if (isset($links[$linkId])) {
                        $buyerItemId = $buyLinkItem->getId();
                        $orderItemId = $orderItem->getId();
                        $buyLinkItemItem = $this->_itemFactory->create()->setPurchasedId(
                            $buyerItemId
                        )->setOrderItemId(
                            $orderItemId
                        );

                        $this->_objectCopyService->copyFieldsetToTarget(
                            downloadable_sales_copy_link::class,
                            'to_purchased',
                            $links[$linkId],
                            $buyLinkItemItem
                        );
                        $linkHash = strtr(
                            base64_encode(
                                microtime() . $buyerItemId . $orderItemId . $product->getId()
                            ),
                            '+/=',
                            '-_,'
                        );
                        $numberOfDownloads = $links[$linkId]->getNumberOfDownloads() * $orderItem->getQtyOrdered();

                        switch ($order->getState()) {
                            case Order::STATE_PAYMENT_REVIEW:
                                $status = Item::LINK_STATUS_PAYMENT_REVIEW;
                                break;
                            case Order::STATE_PENDING_PAYMENT:
                                $status = Item::LINK_STATUS_PENDING_PAYMENT;
                                break;
                            case Order::STATE_COMPLETE:
                                $status = Item::LINK_STATUS_AVAILABLE;
                                break;
                            default:
                                $status = Item::LINK_STATUS_PENDING;
                        }

                        $buyLinkItemItem->setLinkHash(
                            $linkHash
                        )->setNumberOfDownloadsBought(
                            $numberOfDownloads
                        )->setCreatedAt(
                            $orderItem->getCreatedAt()
                        )->setUpdatedAt(
                            $orderItem->getUpdatedAt()
                        )->setStatus(
                            $status
                        )->save();
                    }
                }
            }
        }
    }
}
