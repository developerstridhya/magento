<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Block\Checkout;

use Tridhyatech\OneStepCheckout\Helper\Data as TrDHelper;
use Magento\Customer\Model\AttributeMetadataDataProvider;
use Magento\Ui\Component\Form\AttributeMapper;
use Magento\Checkout\Block\Checkout\AttributeMerger;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Options;
use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Customer\Model\Attribute;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Tridhyatech\OneStepCheckout\Helper\Address as AddressHelper;

class LayoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var TrDHelper
     */
    private $_trdHelper;

    /**
     * @var AttributeMetadataDataProvider
     */
    private $attributeMetadataDataProvider;

    /**
     * @var AttributeMapper
     */
    protected $attributeMapper;

    /**
     * @var AttributeMerger
     */
    protected $merger;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Magento\Customer\Model\Options
     */
    private $options;

    /**
     * @var AddressHelper
     */
    private $_addressHelper;

    /**
     * LayoutProcessor constructor.
     *
     * @param CheckoutSession $checkoutSession
     * @param TrDHelper $trdHelper
     * @param AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param AttributeMapper $attributeMapper
     * @param AttributeMerger $merger
     * @param Magento\Customer\Model\Options $options
     * @param AddressHelper $addressHelper
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        TrDHelper $trdHelper,
        AttributeMetadataDataProvider $attributeMetadataDataProvider,
        AttributeMapper $attributeMapper,
        AttributeMerger $merger,
        \Magento\Customer\Model\Options $options,
        AddressHelper $addressHelper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->_trdHelper = $trdHelper;
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
        $this->options = $options;
        $this->_addressHelper = $addressHelper;
        if (class_exists(\Amazon\Core\Helper\Data::class)) {
            $this->_amazonHelper = ObjectManager::getInstance()->get(\Amazon\Core\Helper\Data::class);
        }
    }

    /**
     * Process Jslayout of block
     *
     * @param array $jsLayout
     *
     * @return array
     *
     * @throws LocalizedException
     */
    public function process($jsLayout)
    {
        if (!$this->_trdHelper->isOneStepPage()) {
            return $jsLayout;
        }
        $isVirtual = $this->checkoutSession->getQuote()->isVirtual();

        /** Billing Component */
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['billingAddress']['children']['billing-address-fieldset']['children'])) {
            $billingaddressFields = $jsLayout['components']['checkout']['children']
            ['steps']['children']['shipping-step']['children']['billingAddress']['children']
            ['billing-address-fieldset']['children'];
            $jsLayout['components']['checkout']['children']['steps']
            ['children']['shipping-step']['children']['billingAddress']
            ['children']['billing-address-fieldset']['children'] =
            $this->getAddressFieldsetNodes($billingaddressFields, 'billingAddress');

            /** Fix the issue of the unsaved vat_id field */
            if (isset($jsLayout['components']['checkout']['children']['steps']
                ['children']['shipping-step']['children']['billingAddress']['children']
                ['billing-address-fieldset']['children']['taxvat'])) {
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['billingAddress']['children']
                ['billing-address-fieldset']['children']['taxvat']['dataScope'] = 'billingAddress.vat_id';
            }
        }

        /** Shipping Component */
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']
            ['children']['shipping-address-fieldset']['children'])) {
            $addressFields = $jsLayout['components']['checkout']['children']['steps']
            ['children']['shipping-step']['children']['shippingAddress']
                      ['children']['shipping-address-fieldset']['children'];
            $jsLayout['components']['checkout']['children']['steps']
            ['children']['shipping-step']['children']['shippingAddress']
            ['children']['shipping-address-fieldset']['children']
                = $this->getAddressFieldsetNodes($addressFields, 'shippingAddress');

            if ($this->_trdHelper->isEnableAmazonPay()) {
                $shippingConfig = &$jsLayout['components']['checkout']['children']
                ['steps']['children']['shipping-step']['children']['shippingAddress'];
                $shippingConfig['component'] = 'Tridhyatech_OneStepCheckout/js/view/shipping';
                $shippingConfig['children']['customer-email']['component'] =
                'Tridhyatech_OneStepCheckout/js/view/form/element/email';
            }

            /** Fix the issue of the unsaved vat_id field */
            if (isset($jsLayout['components']['checkout']['children']
                ['steps']['children']['shipping-step']
                      ['children']['shippingAddress']['children']
                      ['shipping-address-fieldset']['children']['taxvat'])) {
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['shippingAddress']['children']
                ['shipping-address-fieldset']['children']['taxvat']['dataScope'] = 'shippingAddress.vat_id';
            }
        }

        /** Remove billing customer email if quote is not virtual */
        if (!$isVirtual) {
            unset($jsLayout['components']['checkout']['children']['steps']
                ['children']['shipping-step']['children']['billingAddress']['children']['customer-email']);
        }

        /** Remove billing address in payment method content */
        $addressFields = &$jsLayout['components']['checkout']['children']['steps']
        ['children']['billing-step']['children']['payment']['children']['payments-list']['children'];
        foreach ($addressFields as $code => $field) {
            if ($field['component'] === 'Magento_Checkout/js/view/billing-address') {
                unset($addressFields[$code]);
            }
        }

        /** Remove billing address in payment method content when checkout config of display billing address on is set to payment page */
        $jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']['afterMethods']
        ['children']['billing-address-form']['config']['componentDisabled'] = true;

        return $jsLayout;
    }

    /**
     * Address Attributes
     *
     * @return array
     *
     * @throws LocalizedException
     */
    private function getAddressAttributes()
    {
        $attributes = $this->attributeMetadataDataProvider->loadAttributesCollection(
            'customer_address',
            'customer_register_address'
        );

        $addressElements = [
            'custom'  => [],
            'default' => []
        ];
        /** @var Attribute $attribute */
        foreach ($attributes as $attribute) {
            if ($attribute->getIsUserDefined()) {
                continue;
            }
            $code = $attribute->getAttributeCode();
            $element = $this->attributeMapper->map($attribute);
            if (isset($element['label'])) {
                $label = $element['label'];
                $element['label'] = __($label);
            }
            ($attribute->getIsUserDefined()) ?$addressElements['custom'][$code] =
            $element :$addressElements['default'][$code] = $element;
        }

        return $addressElements;
    }
    
    /**
     * Get address fieldset for shipping/billing address
     *
     * @param array $addressFields
     * @param string $type
     *
     * @return array
     *
     * @throws LocalizedException
     */
    public function getAddressFieldsetNodes($addressFields, $type)
    {
        $addressElements = $this->getAddressAttributes();
        $systemAttribute = $addressElements['default'];
        if (count($systemAttribute)) {
            $convertableElem = [
                'prefix' => [$this->options, 'getNamePrefixOptions'],
                'suffix' => [$this->options, 'getNameSuffixOptions'],
            ];
            $systemAttribute = $this->convertElementsToSelect($systemAttribute, $convertableElem);
            $addressFields = $this->merger->merge(
                $systemAttribute,
                'checkoutProvider',
                $type,
                $addressFields
            );
        }

        $customAttribute = $addressElements['custom'];
        if (count($customAttribute)) {
            $addressFields = $this->merger->merge(
                $customAttribute,
                'checkoutProvider',
                $type . '.custom_attributes',
                $addressFields
            );
        }

        $this->addCustomerAttributes($addressFields, $type);
        $this->addAddressClasses($addressFields);

        return $addressFields;
    }

    /**
     * Modify Field Street
     *
     * @param array $addressFields
     *
     * @return $this
     */
    public function modifyFieldStreet($addressFields)
    {
        if ($this->_trdHelper->isLatestLayout()) {
            $addressFields['country_id']['config']['template'] = 'Tridhyatech_OneStepCheckout/formdata/form/field';
            $addressFields['region_id']['config']['template'] = 'Tridhyatech_OneStepCheckout/formdata/form/field';
            foreach ($addressFields['street']['children'] as $key => $value) {
                $addressFields['street']['children'][0]['label'] = $addressFields['street']['label'];
                $addressFields['street']['children'][$key]['config']['template']
                = 'Tridhyatech_OneStepCheckout/formdata/form/field';
            }
            $addressFields['street']['config']['fieldTemplate'] = 'Tridhyatech_OneStepCheckout/formdata/form/field';
        }

        return $this;
    }

    /**
     * Add customer attribute like gender, dob, taxvat
     *
     * @param array $addressFields
     * @param string $type
     *
     * @return $this
     *
     * @throws LocalizedException
     */
    private function addCustomerAttributes($addressFields, $type)
    {
        $addressElements = [];
        $attributes = $this->attributeMetadataDataProvider->loadAttributesCollection(
            'customer',
            'customer_account_create'
        );
        
        foreach ($attributes as $attribute) {
            if (!$this->_addressHelper->isCustomerAttributeVisible($attribute)) {
                continue;
            }
            $addressElements[$attribute->getAttributeCode()] = $this->attributeMapper->map($attribute);
        }

        if (count($addressElements)) {
            $addType = $type . '.custom_attributes';
            $addressFields = $this->merger->merge(
                $addressElements,
                'checkoutProvider',
                $addType,
                $addressFields
            );
        }

        foreach ($addressFields as $fieldCode => &$value) {
            if (isset($value['label'])) {
                $value['label'] = __($value['label']);
            }
        }

        return $this;
    }

    /**
     * Address Classes
     *
     * @param array $addressFields
     *
     * @return $this
     */
    private function addAddressClasses(&$addressFields)
    {
        $fieldPosition = $this->_addressHelper->getAddressFieldPosition();

        $attributes = [];
        $allFields = $this->_addressHelper->getSortedField(false);
        foreach ($allFields as $allfield) {
            foreach ($allfield as $field) {
                $attributes[] = $field->getAttributeCode();
            }
        }

        $this->modifyFieldStreet($addressFields);

        foreach ($addressFields as $code => &$field) {
            $fieldContent = isset($fieldPosition[$code]) ? $fieldPosition[$code] : [];
            if (empty($fieldContent)) {
                unset($addressFields[$code]);
            }
            if (!count($fieldContent)) {
                if (in_array($code, ['country_id'])) {
                    $field['config']['additionalClasses'] = "trd-hidden";
                    continue;
                } elseif (in_array($code, $attributes)) {
                    unset($addressFields[$code]);
                }
            } else {
                $elementClass = isset($field['config']['additionalClasses'])
                ? $field['config']['additionalClasses'] : '';
                $field['config']['additionalClasses'] =
                "{$elementClass} col-ones trd-{$fieldContent['colspan']}" .
                 ($fieldContent['isNewRow'] ? ' trd-clear' : '');
                $field['sortOrder'] = (isset($field['sortOrder']) && !in_array($code, $attributes)) ?
                $field['sortOrder'] : $fieldContent['sortOrder'];
                if ($code === 'dob') {
                    $field['options'] =
                    ['yearRange' => '-120y:c+nn', 'maxDate' => '-1d',
                     'changeMonth' => true, 'changeYear' => true];
                    $field['component'] = 'Magento_Ui/js/form/element/date';
                }

                $this->modifyTemplate($field);
            }
        }

        /**
         * Compatible with Amazon Pay
         */
        if ($this->_trdHelper->isEnableAmazonPay()) {
            if (class_exists(\Amazon\Core\Helper\Data::class)) {
                if ($this->_amazonHelper->isPwaEnabled()) {
                    $addressFields['inline-form-manipulator'] = [
                        'component' => 'Tridhyatech_OneStepCheckout/js/view/amazon'
                    ];
                }
            }
        }

        return $this;
    }

    /**
     * Convert addressElements(like prefix and suffix) from inputs to selects when necessary
     *
     * @param array $addressElements address attributes
     * @param array $convertableElem addressFields and their callbacks
     *
     * @return array
     */
    private function convertElementsToSelect($addressElements, $convertableElem)
    {
        $codes = array_keys($convertableElem);
        foreach (array_keys($addressElements) as $code) {
            
            if (!in_array($code, $codes)) {
                continue;
            }

            $options = call_user_func($convertableElem[$code]);

            if (!is_array($options)) {
                continue;
            }

            $addressElements[$code]['dataType'] = 'select';
            $addressElements[$code]['formElement'] = 'select';

            foreach ($options as $key => $value) {
                $addressElements[$code]['options'][] = [
                    'value' => $key,
                    'label' => $value,
                ];
            }
        }

        return $addressElements;
    }

    /**
     * Change template to remove valueUpdate = 'keyup'
     *
     * @param string $field
     * @param string $template
     *
     * @return $this
     */
    public function modifyTemplate(&$field, $template = 'Tridhyatech_OneStepCheckout/formdata/form/element/input')
    {
        if (isset($field['type']) && $field['type'] === 'group') {
            foreach ($field['children'] as $key => &$child) {
                if ($key == 0 &&
                    in_array('street', explode('.', $field['dataScope'])) &&
                    $this->_trdHelper->isGoogleHttps()
                ) {
                    $this->modifyTemplate($child, 'Tridhyatech_OneStepCheckout/formdata/form/element/street');
                    continue;
                }
                $this->modifyTemplate($child);
            }
        } elseif (isset($field['config']['elementTmpl']) &&
            $field['config']['elementTmpl'] === "ui/form/element/input") {
            $field['config']['elementTmpl'] = $template;
            if ($this->_trdHelper->isLatestLayout()) {
                $field['config']['template'] = 'Tridhyatech_OneStepCheckout/formdata/form/field';
            }
        }

        return $this;
    }
}
