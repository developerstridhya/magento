<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Cms\Block\Block;
use Tridhyatech\OneStepCheckout\Helper\Data as TrDHelper;
use Tridhyatech\OneStepCheckout\Model\System\Config\Source\CmsBlockPosition;

class Cms extends Template
{
    /**
     * @var BlockRepositoryInterface
     */
    protected $blockRepository;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var TrDHelper
     */
    protected $_trdHelper;

    /**
     * @var cmsPosition
     */
    protected $_cmsPosition;

    /**
     * Cms constructor.
     *
     * @param Context $context
     * @param BlockRepositoryInterface $blockRepository
     * @param CheckoutSession $checkoutSession
     * @param TrDHelper $trdHelper
     * @param CmsBlockPosition $cmsPosition
     * @param array $data
     */
    public function __construct(
        Context $context,
        BlockRepositoryInterface $blockRepository,
        CheckoutSession $checkoutSession,
        TrDHelper $trdHelper,
        CmsBlockPosition $cmsPosition,
        array $data = []
    ) {
       
        $this->blockRepository = $blockRepository;
        $this->checkoutSession = $checkoutSession;
        $this->_trdHelper = $trdHelper;
        $this->_cmsPosition = $cmsPosition;

        parent::__construct($context, $data);
    }

    /**
     * Retrive Cms Block
     *
     * @return array
     */
    public function getCmsBlocks()
    {
        $result = [];
        $blockList = $this->_trdHelper->isEnableMainStatic() ? $this->_trdHelper->getConfigBlockList() : [];

        foreach ($blockList as $k => $value) {
           
            if (($value['position'] == $this->_cmsPosition::SHOW_AT_SUCCESS_PAGE
                && $this->getNameInLayout() == 'trd.mainstatic-block.success')
                || ($value['position'] == $this->_cmsPosition::SHOW_AT_TOP_CHECKOUT_PAGE
                    && $this->getNameInLayout() == 'trd.mainstatic-block.top')
                || ($value['position'] == $this->_cmsPosition::SHOW_AT_BOTTOM_CHECKOUT_PAGE
                    && $this->getNameInLayout() == 'trd.mainstatic-block.bottom')) {

                $result[] = [
                    'content'   => $this->getLayout()
                    ->createBlock(\Magento\Cms\Block\Block::class)
                    ->setBlockId($value['block'])->toHtml(),
                    'sortOrder' => $value['sort_order']
                ];
            }
        }
        
        usort($result, function ($a, $b) {
            return ($a['sortOrder'] <= $b['sortOrder']) ? -1 : 1;
        });
        
        return $result;
    }
}
