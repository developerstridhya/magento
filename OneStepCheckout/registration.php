<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Tridhyatech_OneStepCheckout',
    __DIR__
);
