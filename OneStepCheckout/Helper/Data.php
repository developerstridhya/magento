<?php
/**
 * DISCLAIMER
 *
 *
 * @category    Tridhyatech
 * @package     Tridhyatech_OneStepCheckout
 * @copyright   Tridhyatech Limited (https://www.tridhyatech.com/)
 * @license     https://www.tridhyatech.com//license.html
 */

namespace Tridhyatech\OneStepCheckout\Helper;

use Magento\Downloadable\Model\Product\Type;
use Magento\Downloadable\Observer\IsAllowedGuestCheckoutObserver;
use Magento\Framework\View\DesignInterface;
use Tridhyatech\OneStepCheckout\Model\System\Config\Source\Position;
use Zend_Serializer_Exception;
use Magento\Framework\App\ProductMetadataInterface;
use Exception;
use Magento\Backend\App\Config;
use Magento\Framework\App\Area;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\State;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class Data extends AbstractHelper
{
    public const CONFIG_MODULE_PATH = 'onestepcheckout';
    public const CONFIG_PATH_DISPLAY = 'display_configuration';
    public const CONFIG_PATH_DESIGN = 'design_configuration';
    public const CONFIG_PATH_BLOCK = 'block_configuration';
    public const CONFIG_ROUTE_PATH = 'onestepcheckout';
    public const CONFIG_PATH_DELIVERY = 'delivery';

    /**
     * @var array
     */
    protected $_data = [];

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var Config
     */
    protected $backendConfig;

    /**
     * @var array
     */
    protected $isArea = [];

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param CheckoutSession $checkoutSession
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        CheckoutSession $checkoutSession,
        TimezoneInterface $timezone
    ) {
        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->timezone = $timezone;

        parent::__construct($context);
    }

    /**
     * @var bool OneStep Method Register
     */
    protected $_register = false;

    /**
     * Get Route Config
     *
     * @param string $store
     *
     * @return mixed|string
     */
    public function getRouteConfig($store = null)
    {
        $route = $this->getConfigGeneral('route', $store);

        return !empty($route) ? $route : self::CONFIG_ROUTE_PATH;
    }

    /**
     * Is Virtual
     *
     * @return bool
     */
    public function isVirtual()
    {
        return $this->checkoutSession->getQuote()->isVirtual();
    }
    
    /**
     * Is Enable Google Api
     *
     * @return bool
     */
    public function isEnableGoogleApi()
    {
        return $this->getAutoDetectedAddress() == 'google';
    }

    /**
     * Is Enable
     *
     * @param string $storeId
     *
     * @return bool
     */
    public function isEnabled($storeId = null)
    {
        return $this->getConfigGeneral('enabled', $storeId);
    }

    /**
     * Sorted Checkout Field
     *
     * @param string $storeId
     *
     * @return mixed
     */
    public function getSortedCheckoutFields($storeId = null)
    {
        return $this->getConfigValue(static::CONFIG_MODULE_PATH . '/layouts/onestep_fields', $storeId);
    }
    
    /**
     * Module Config
     *
     * @param string $field
     * @param string $storeId
     * @param string $type
     *
     * @return mixed
     */
    public function getModuleConfig($field = '', $storeId = null, $type = "")
    {
        $field = ($field !== '') ? '/' . $field : '';
        if ($type) {
            switch ($type) {
                case 'display':
                    $path =  static::CONFIG_MODULE_PATH .'/'. static::CONFIG_PATH_DISPLAY . $field;
                    return $this->getConfigValue($path, $storeId);
                case 'design':
                    $path =  static::CONFIG_MODULE_PATH .'/'. static::CONFIG_PATH_DESIGN . $field;
                    return $this->getConfigValue($path, $storeId);
                break;
            }
        }
        
        return $this->getConfigValue(static::CONFIG_MODULE_PATH . $field, $storeId);
    }

    /**
     * Config Value
     *
     * @param string $field
     * @param string $scopeValue
     * @param string $scopeType
     *
     * @return array|mixed
     */
    public function getConfigValue($field, $scopeValue = null, $scopeType = ScopeInterface::SCOPE_STORE)
    {
        if ($scopeValue === null && !$this->isArea()) {
            /** @var Config $backendConfig */
            if (!$this->backendConfig) {
                $this->backendConfig = $this->objectManager->get(\Magento\Backend\App\ConfigInterface::class);
            }

            return $this->backendConfig->getValue($field);
        }

        return $this->scopeConfig->getValue($field, $scopeType, $scopeValue);
    }

    /**
     * Get Data
     *
     * @param string $name
     *
     * @return null
     */
    public function getData($name)
    {
        if (array_key_exists($name, $this->_data)) {
            return $this->_data[$name];
        }

        return null;
    }

    /**
     * Set Data
     *
     * @param string $name
     * @param string $value
     *
     * @return $this
     */
    public function setData($name, $value)
    {
        $this->_data[$name] = $value;

        return $this;
    }

    /**
     * Get Current Url
     *
     * @return mixed
     */
    public function getCurrentUrl()
    {
        $model = $this->objectManager->get(UrlInterface::class);

        return $model->getCurrentUrl();
    }

    /**
     * Get Version Compare
     *
     * @param string $ver
     * @param string $operator
     *
     * @return mixed
     */
    public function versionCompare($ver, $operator = '>=')
    {
        $productMetadata = $this->objectManager->get(ProductMetadataInterface::class);
        $version = $productMetadata->getVersion(); //will return the magento version

        return version_compare($version, $ver, $operator);
    }

    /**
     * Serialize
     *
     * @param array $data
     *
     * @return string
     */
    public function serialize($data)
    {
        if ($this->versionCompare('2.2.0')) {
            return self::jsonEncode($data);
        }

        return $this->getSerializeClass()->serialize($data);
    }

    /**
     * Unserialize
     *
     * @param string $string
     *
     * @return mixed
     */
    public function unserialize($string)
    {
        if ($this->versionCompare('2.2.0')) {
            return self::jsonDecode($string);
        }

        return $this->getSerializeClass()->unserialize($string);
    }

    /**
     * Encode the mixed $valueToEncode into the JSON format
     *
     * @param mixed $valueToEncode
     *
     * @return string
     */
    public static function jsonEncode($valueToEncode)
    {
        try {
            $encodeValue = self::getJsonHelper()->jsonEncode($valueToEncode);
        } catch (Exception $e) {
            $encodeValue = '{}';
        }

        return $encodeValue;
    }

    /**
     * Decodes the given $encodedValue string which is encoded in the JSON format
     *
     * @param string $encodedValue
     *
     * @return mixed
     */
    public static function jsonDecode($encodedValue)
    {
        try {
            $decodeValue = self::getJsonHelper()->jsonDecode($encodedValue);
        } catch (Exception $e) {
            $decodeValue = [];
        }

        return $decodeValue;
    }

    /**
     * Is Admin Store
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->isArea(Area::AREA_ADMINHTML);
    }

    /**
     * Area
     *
     * @param string $area
     *
     * @return mixed
     */
    public function isArea($area = Area::AREA_FRONTEND)
    {
        if (!isset($this->isArea[$area])) {
            /** @var State $state */
            $state = $this->objectManager->get(\Magento\Framework\App\State::class);

            try {
                $this->isArea[$area] = ($state->getAreaCode() == $area);
            } catch (Exception $e) {
                $this->isArea[$area] = false;
            }
        }

        return $this->isArea[$area];
    }

    /**
     * Create Object
     *
     * @param string $path
     * @param array $arguments
     *
     * @return mixed
     */
    public function createObject($path, $arguments = [])
    {
        return $this->objectManager->create($path, $arguments);
    }

    /**
     * Get Object
     *
     * @param string $path
     *
     * @return mixed
     */
    public function getObject($path)
    {
        return $this->objectManager->get($path);
    }

    /**
     * Get Json Helper
     *
     * @return JsonHelper|mixed
     */
    public static function getJsonHelper()
    {
        return ObjectManager::getInstance()->get(JsonHelper::class);
    }

    /**
     * Get Serialize Class
     *
     * @return mixed
     */
    protected function getSerializeClass()
    {
        return $this->objectManager->get('Zend_Serializer_Adapter_PhpSerialize');
    }

    /**
     * Check the current page is onestepcheckout
     *
     * @param string $store
     *
     * @return bool
     */
    public function isOneStepPage($store = null)
    {
        $moduleEnable = $this->isEnabled($store);

        return $moduleEnable && ($this->_request->getRouteName() === self::CONFIG_ROUTE_PATH);
    }

    /**
     * Is Register
     *
     * @return bool
     */
    public function isRegister()
    {
        return $this->_register;
    }

    /**
     * Set Register
     *
     * @param bool $flag
     */
    public function setRegister($flag)
    {
        $this->_register = $flag;
    }

    /**
     * Config General
     *
     * @param string $code
     * @param string $storeId
     *
     * @return mixed
     */
    public function getConfigGeneral($code = '', $storeId = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValue(static::CONFIG_MODULE_PATH . '/general' . $code, $storeId);
    }

    /**
     * Get magento default country
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getDefaultCountryId($store = null)
    {
        return $this->objectManager->get(\Magento\Directory\Helper\Data::class)->getDefaultCountry($store);
    }

    /**
     * Get page title
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getCheckoutTitle($store = null)
    {
        return $this->getConfigGeneral('title', $store) ?: 'One Step Checkout';
    }

    /**
     * Get page description
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getCheckoutDescription($store = null)
    {
        return $this->getConfigGeneral('description', $store);
    }

    /**
     * Default shipping method
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getDefaultShippingMethod($store = null)
    {
        return $this->getConfigGeneral('default_shipping_method', $store);
    }

    /**
     * Allow guest checkout
     *
     * @param mixed $quote
     * @param string $store
     *
     * @return bool
     */
    public function getAllowGuestCheckout($quote, $store = null)
    {
        $allowGuestCheckout = boolval($this->getConfigGeneral('allow_guest_checkout', $store));

        $allowGuestCheckoutflag = $this->scopeConfig->isSetFlag(
            'catalog/downloadable/disable_guest_checkout',
            ScopeInterface::SCOPE_STORE,
            $store
        );
        if ($allowGuestCheckoutflag) {
            foreach ($quote->getAllItems() as $item) {
                if (($product = $item->getProduct()) && $product->getTypeId() == Type::TYPE_DOWNLOADABLE) {
                    return false;
                }
            }
        }

        return $allowGuestCheckout;
    }

    /**
     * Default payment method
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getDefaultPaymentMethod($store = null)
    {
        return $this->getConfigGeneral('default_payment_method', $store);
    }

    /**
     * Redirect To OneStepCheckout
     *
     * @param string $store
     *
     * @return bool
     */
    public function isRedirectToOneStepCheckout($store = null)
    {
        return boolval($this->getConfigGeneral('redirect_to_one_step_checkout', $store));
    }

    /**
     * Show billing address
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getShowBillingAddress($store = null)
    {
        return boolval($this->getConfigGeneral('show_billing_address', $store));
    }

    /**
     * Google api key
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getGoogleApiKey($store = null)
    {
        return $this->getConfigGeneral('google_api_key', $store);
    }

    /**
     * Google specific allowed country list
     *
     * @param string $store
     *
     * @return string
     */
    public function getGoogleSpecificCountries($store = null)
    {
        return $this->getConfigGeneral('google_specific_country', $store);
    }

    /**
     * Check if the page is https
     *
     * @return bool
     */
    public function isGoogleHttps()
    {
        $isEnable = ($this->getAutoDetectedAddress() == 'google');

        return $isEnable && $this->_request->isSecure();
    }

    /**
     * Get auto detected address
     *
     * @param string $store
     *
     * @return null|'google'
     */
    public function getAutoDetectedAddress($store = null)
    {
        $auto = $this->getConfigGeneral('auto_detect_address', $store);
        if ($auto == 'google' && $this->getGoogleApiKey() != "") {
            return $auto;
        }
        return "";
    }

    /**
     * Show Toc
     *
     * @param string $store
     *
     * @return mixed
     */
    public function getShowTOC($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_tc', $store, 'display');
    }

    /**
     * Term and condition checkbox in payment block will be not visible if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function disabledPaymentTOC($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_tc', $store, 'display') != Position::SHOW_IN_PAYMENT;
    }

    /**
     * Enable Terms And Condition
     *
     * @param string $store
     *
     * @return mixed
     */
    public function isEnabledTermsAndCondition($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_tc', $store, 'display') != Position::NOT_SHOW;
    }

    /**
     * Term and condition checkbox in review will be not visible if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function disabledReviewTermsAndConditionSection($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_tc', $store, 'display') != Position::SHOW_IN_REVIEW;
    }

    /**
     * Login link will be hidden if this function return true
     *
     * @param string $store
     *
     * @return bool
     */
    public function isDisableAuthentication($store = null)
    {
        return !$this->getModuleConfig('enable_disable/is_enabled_login', $store, 'display');
    }

    /**
     * Show Item detail when this function return 'true'
     *
     * @param string $store
     *
     * @return bool
     */
    public function isDisabledReviewCartSection($store = null)
    {
        return !$this->getModuleConfig('is_enabled_review_cart_section', $store, 'display');
    }

    /**
     * Show list toggle when this function return 'true'
     *
     * @param string $store
     *
     * @return bool
     */
    public function isShowItemListToggle($store = null)
    {
        return boolval($this->getModuleConfig('is_show_item_list_toggle', $store, 'display'));
    }

    /**
     * Hide product image onestepcheckout page
     *
     * @param string $store
     *
     * @return bool
     */
    public function isHideProductImage($store = null)
    {
        return !$this->getModuleConfig('is_show_product_image', $store, 'display');
    }

    /**
     * Coupon will be not visible if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function isdisabledReviewCoupon($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_couponcode', $store, 'display') != Position::SHOW_IN_REVIEW;
    }

    /**
     * Coupon will be not visible if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function disabledPaymentCoupon($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_couponcode', $store, 'display') != Position::SHOW_IN_PAYMENT;
    }

    /**
     * Order Comment will be not visible in payment area if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function isDisabledOrderCommentPayment($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_order_comment', $store, 'display')
        != Position::SHOW_IN_PAYMENT;
    }

    /**
     * Order Comment will be not visible in review area if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function isDisabledOrderCommentReview($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_order_comment', $store, 'display')
        != Position::SHOW_IN_REVIEW;
    }

    /**
     * GiftMessage will be not visible if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function isDisabledGiftMessage($store = null)
    {
        return $this->getModuleConfig('enable_disable/show_gift_message', $store, 'display');
    }

    /**
     * Gift message items
     *
     * @param string $store
     *
     * @return bool
     */
    public function isEnableGiftMessageItems($store = null)
    {
        return (bool)$this->getModuleConfig('enable_disable/show_gift_message_items', $store, 'display');
    }

    /**
     * Newsleter block will be not visible if this function return 'true'
     *
     * @param string $store
     *
     * @return mixed
     */
    public function isDisabledNewsletter($store = null)
    {
        return !$this->getModuleConfig('is_enabled_newsletter', $store, 'display');
    }

    /**
     * Is newsleter subcribed default
     *
     * @param string $store
     *
     * @return mixed
     */
    public function isSubscribedByDefault($store = null)
    {
        return (bool)$this->getModuleConfig('is_checked_newsletter', $store, 'display');
    }

    /**
     * Get layout tempate: 1 or 2 or 3 columns
     *
     * @param string $store
     *
     * @return string
     */
    public function getLayoutTypeTemplate($store = null)
    {
        return 'Tridhyatech_OneStepCheckout/page-types/' . $this->getModuleConfig('page_layout', $store, 'design');
    }

    /**
     * Design Configuration
     *
     * @param string $code
     * @param string $store
     *
     * @return mixed
     */
    public function getConfigDesign($code = '', $store = null)
    {
        $code = $code ? self::CONFIG_PATH_DESIGN . '/' . $code : self::CONFIG_PATH_DESIGN;

        return $this->getModuleConfig($code, $store);
    }

    /**
     * Latest Layout
     *
     * @return bool
     */
    public function isLatestLayout()
    {
        $store = null;
        return $this->getModuleConfig('page_design', $store, 'design') == 'latest' ? true : false;
    }

    /**
     * CMS Static Block Configuration
     *
     * @param string $code
     * @param string $store
     *
     * @return mixed
     */
    public function getStaticBlockConfig($code = '', $store = null)
    {
        $code = $code ? self::CONFIG_PATH_BLOCK . '/' . $code : self::CONFIG_PATH_BLOCK;

        return $this->getModuleConfig($code, $store);
    }

    /**
     * Enable Main Static
     *
     * @param string $store
     *
     * @return bool
     */
    public function isEnableMainStatic($store = null)
    {
        return $this->getStaticBlockConfig('is_enabled_block', $store);
    }
    
    /**
     * Config Block List
     *
     * @param string $stores
     *
     * @return mixed
     * @throws Zend_Serializer_Exception
     */
    public function getConfigBlockList($stores = null)
    {
        return $this->unserialize($this->getStaticBlockConfig('list', $stores));
    }

    /**
     * Enable Amazon Pay
     *
     * @return bool
     */
    public function isEnableAmazonPay()
    {
        return $this->isModuleOutputEnabled('Amazon_Payment');
    }

    /**
     * Is Show Copyright
     *
     * @param string $store
     *
     * @return bool
     */
    public function isShowCopyright($store = null)
    {
        return $this->getModuleConfig('enable_disable/copyright', $store, 'display');
    }

    /**
     * Is Show Header Footer
     *
     * @param string $store
     *
     * @return bool
     */
    public function isShowHeaderFooter($store = null)
    {
        return $this->getModuleConfig('enable_disable/is_display_fh', $store, 'display');
    }

    /**
     * Delivery Date Configuration
     *
     * @param string $code
     * @param string $store
     *
     * @return mixed
     */
    public function getDeliveryConfig($code = '', $store = null)
    {
        $code = $code ? self::CONFIG_PATH_DELIVERY . '/' . $code : self::CONFIG_PATH_DELIVERY;

        return $this->getModuleConfig($code, $store);
    }

    /**
     * Delivery will be hidden if this function return true
     *
     * @param string $store
     *
     * @return bool
     */
    public function isDisableDelivery($store = null)
    {
        return !(bool)$this->getModuleConfig('delivery/enabled', $store);
    }

    /**
     * Returns array of delivery time
     *
     * @return array
     */
    public function getDeliveryTimeOptions()
    {
        $options[] = ['label' => __('Select Delivery Time'),'value' => ""];
        $delivery = $this->getDeliveryConfig();
        $interval = $delivery['interval'];
        $starttime = $delivery['from_hours'];
        $endtime = $delivery['to_hours'];
        $duration = $delivery['interval'];
         
        $start = new \DateTime($starttime);
        $end = new \DateTime($endtime);
        $startntime = $start->format('H:i');
        $endntime = $end->format('H:i');
        $i=0;
        $time = [];
        while (strtotime($startntime) <= strtotime($endntime)) {
            $start = $startntime;
            $end = date('H:i', strtotime('+'.$duration.' minutes', strtotime($startntime)));
            $startntime = date('H:i', strtotime('+'.$duration.' minutes', strtotime($startntime)));
            $i++;
            if (strtotime($startntime) <= strtotime($endntime)) {
                $len = $start." - ".$end;
                $lenlabel = date("h:i A", strtotime($start))." - ".date("h:i A", strtotime($end));
                $len = date("H:i", strtotime($start))."-".date("H:i", strtotime($end));
                $options[$i]['label'] = $lenlabel;
                $options[$i]['value'] = $len;
            }
        }
        return $options;
    }

    /**
     * Selected delivery fields will be required if this function return true
     *
     * @param string $store
     *
     * @return bool
     */
    public function deliveryRequired($store = null)
    {
        if ($this->getModuleConfig('delivery/required', $store) != "") {
            return false;
        }
        return true;
    }

    /**
     * Change date format
     *
     * @param string $date
     *
     * @return string
     */
    public function convertDate($date)
    {
        $dateTimeZone = $this->timezone->date(new \DateTime($date))->format('jS F Y');
        return $dateTimeZone;
    }

    /**
     * Returns array of disabled date
     *
     * @param string $store
     * @return array
     */
    public function getExcludeDates($store = null)
    {
        $dates = [];
        $excludeDates = $this->getModuleConfig('delivery/disabled_day', $store);
        if (empty($excludeDates)) {
            return [];
        }
        if (! $values = $this->unserialize($excludeDates)) {
            return [];
        }
        foreach ($values as $value) {
            if (!isset($value['date'])) {
                continue;
            }
            if (in_array($value['date'], $dates)) {
                continue;
            }
            array_push($dates, $value['date']);
        }
        return $dates;
    }

    /**
     * Returns label from value
     *
     * @param string $value
     *
     * @return string
     */
    public function getDeliveryTimeLabel($value)
    {
        $returnLabel = "";
        if ($value != "") {
            $split = explode("-", $value);
            if ($this->isAdmin()) {
                if (isset($split[0])) {
                    $returnLabel = date("H:i A", strtotime($split[0]));
                }
                if (isset($split[1])) {
                    $returnLabel .= " - ".date("H:i A", strtotime($split[1]));
                }
            } else {
                if (isset($split[0])) {
                    $returnLabel = date("g:i A", strtotime($split[0]));
                }
                if (isset($split[1])) {
                    $returnLabel .= " - ".date("g:i A", strtotime($split[1]));
                }
            }
        }
        return $returnLabel;
    }
}
