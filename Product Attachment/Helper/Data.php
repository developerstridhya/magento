<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_ProductAttachment
 */

namespace Tridhyatech\ProductAttachment\Helper;

/**
 * Tridhyatech_ProductAttachment Data Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Helper
     *
     * @param mixed $needle
     * @param mixed $haystack
     * @param mixed $strict = false
     */
    public function checkInArrayR($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->checkInArrayR($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }
}
