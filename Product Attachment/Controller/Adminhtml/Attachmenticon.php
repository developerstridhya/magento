<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_ProductAttachment
 */

namespace Tridhyatech\ProductAttachment\Controller\Adminhtml;

/**
 * Class Attachmenticon
 */
abstract class Attachmenticon extends \Magento\Backend\App\Action
{
    public const ADMIN_RESOURCE = 'Tridhyatech_ProductAttachment::top_level';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Tridhyatech'), __('Tridhyatech'))
            ->addBreadcrumb(__('Attachment Icons'), __('Attachment Icons'));
        return $resultPage;
    }
}
