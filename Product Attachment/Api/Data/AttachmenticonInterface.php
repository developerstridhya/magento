<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_ProductAttachment
 */

namespace Tridhyatech\ProductAttachment\Api\Data;

/**
 * Interface AttachmenticonInterface
 */
interface AttachmenticonInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    public const ICON_TYPE = 'icon_type';
    public const ATTACHMENTICON_ID = 'attachmenticon_id';
    public const FILE_EXTENSION = 'file_extension';
    public const STATUS = 'status';
    public const ICON = 'icon';

    /**
     * Get attachmenticon_id
     *
     * @return string|null
     */
    public function getAttachmenticonId();

    /**
     * Set attachmenticon_id
     *
     * @param string $attachmenticonId
     *
     * @return \Tridhyatech\ProductAttachment\Api\Data\AttachmenticonInterface
     */
    public function setAttachmenticonId($attachmenticonId);

    /**
     * Get icon_type
     *
     * @return string|null
     */
    public function getIconType();

    /**
     * Set icon_type
     *
     * @param string $iconType
     *
     * @return \Tridhyatech\ProductAttachment\Api\Data\AttachmenticonInterface
     */
    public function setIconType($iconType);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Tridhyatech\ProductAttachment\Api\Data\AttachmenticonExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Get icon
     *
     * @return string|null
     */
    public function getIcon();

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return \Tridhyatech\ProductAttachment\Api\Data\ProductattachmentInterface
     */
    public function setIcon($icon);

    /**
     * Get file_extension
     *
     * @return string|null
     */
    public function getFileExtension();

    /**
     * Set file_extension
     *
     * @param string $fileExtension
     *
     * @return \Tridhyatech\ProductAttachment\Api\Data\ProductattachmentInterface
     */
    public function setFileExtension($fileExtension);

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return \Tridhyatech\ProductAttachment\Api\Data\ProductattachmentInterface
     */
    public function setStatus($status);

    /**
     * Set an extension attributes object.
     *
     * @param \Tridhyatech\ProductAttachment\Api\Data\AttachmenticonExtensionInterface $extensionAttributes
     *
     * @return $this
     */
    public function setExtensionAttributes(
        \Tridhyatech\ProductAttachment\Api\Data\AttachmenticonExtensionInterface $extensionAttributes
    );
}
