<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_ProductAttachment
 */
namespace Tridhyatech\ProductAttachment\Model;

use Tridhyatech\ProductAttachment\Api\Data\AttachmenticonInterface;
use Tridhyatech\ProductAttachment\Api\Data\AttachmenticonInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Attachmenticon extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var AttachmenticonInterfaceFactory
     */
    protected $attachmenticonDataFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var $_eventPrefix
     */
    protected $_eventPrefix = 'tridhyatech_productattachment_attachmenticon';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param AttachmenticonInterfaceFactory $attachmenticonDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Tridhyatech\ProductAttachment\Model\ResourceModel\Attachmenticon $resource
     * @param \Tridhyatech\ProductAttachment\Model\ResourceModel\Attachmenticon\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        AttachmenticonInterfaceFactory $attachmenticonDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Tridhyatech\ProductAttachment\Model\ResourceModel\Attachmenticon $resource,
        \Tridhyatech\ProductAttachment\Model\ResourceModel\Attachmenticon\Collection $resourceCollection,
        array $data = []
    ) {
        $this->attachmenticonDataFactory = $attachmenticonDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve attachmenticon model with attachmenticon data
     *
     * @return AttachmenticonInterface
     */
    public function getDataModel()
    {
        $attachmenticonData = $this->getData();
        
        $attachmenticonDataObject = $this->attachmenticonDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $attachmenticonDataObject,
            $attachmenticonData,
            AttachmenticonInterface::class
        );
        
        return $attachmenticonDataObject;
    }
}
