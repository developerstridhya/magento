<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_ProductAttachment
 */
namespace Tridhyatech\ProductAttachment\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Asset\Repository;
use Tridhyatech\ProductAttachment\Model\ResourceModel\Attachmenticon\CollectionFactory as AttachmenticonColFactory;

class Attachmentfile extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Repository
     */
    private $assetRepo;

    /**
     * @var AttachmenticonColFactory
     */
    protected $attachmenticonCollectionFactory;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param StoreManagerInterface $storeManager
     * @param AttachmenticonColFactory $attachmenticonCollectionFactory
     * @param Repository $assetRepo
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StoreManagerInterface $storeManager,
        AttachmenticonColFactory $attachmenticonCollectionFactory,
        Repository $assetRepo,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->storeManager = $storeManager;
        $this->attachmenticonCollectionFactory = $attachmenticonCollectionFactory;
        $this->assetRepo = $assetRepo;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $path = $this->storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'productattachment/icon/';
            $filepath = $this->storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'productattachment/attachment_file/';
            $baseImage = $this->assetRepo->getUrl('Tridhyatech_ProductAttachment::images/productattachment.png');
            $linkImage = $this->assetRepo->getUrl('Tridhyatech_ProductAttachment::images/link-icon.png');
            
            foreach ($dataSource['data']['items'] as & $item) {
                if ($item['attachment_file']) {
                    $imageicon = $baseImage;
                    $collection = $this->attachmenticonCollectionFactory->create();
                    $collection = $collection->addFieldToFilter('status', 1);
                    $iconpath = $item['attachment_file'];
                    // $ext = pathinfo($iconpath, PATHINFO_EXTENSION);
                    $pathArray = explode('.', $iconpath);
                    $ext = end($pathArray);
                    if (!empty($collection->getData())) {
                        foreach ($collection->getData() as $key => $value) {
                            if (strpos($value['file_extension'], $ext) !== false) {
                                $imageicon = $path.$value['icon'];
                            }
                        }
                    }

                    $item['attachment_file' . '_src'] = $imageicon;
                    $item['attachment_file' . '_alt'] = $item['attachment_name'];
                    $item['attachment_file' . '_orig_src'] = $filepath.$item['attachment_file'];
                } elseif ($item['attachment_link']) {
                    $item['attachment_file' . '_src'] = $linkImage;
                    $item['attachment_file' . '_alt'] = 'Icon';
                    $item['attachment_file' . '_orig_src'] = $linkImage;
                } else {
                    $item['attachment_file' . '_src'] = $baseImage;
                    $item['attachment_file' . '_alt'] = 'Icon';
                    $item['attachment_file' . '_orig_src'] = $baseImage;
                }
            }
        }
        return $dataSource;
    }
}
