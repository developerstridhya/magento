# Product Attachement By Tridhya Tech

## Description

Product Attachment is the key feature to let your buyers know detailed information about the product that they are going to buy. Admin can convey this information in the form of an attachment to the product. This attachment can be anything like a brochure, catalog, user manual, etc.

This Product Attachment extension allows the admin to add an attachment to the product. Using this extension, the admin can add an attachment to the specific customer groups. Apart from the attachment, there is one functionality to add attachment icons.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/module-productattachment
        * php bin/magento module:enable Tridhyatech_ProductAttachment
        * php bin/magento setup:upgrade
