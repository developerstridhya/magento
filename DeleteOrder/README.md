# Delete Order By Tridhya Tech

## Description

Deleting old test orders, shipments, invoices, and credit memos data is very important for the system admin before making their website Live. Also, there is no way to delete mistakenly created invoice, shipment, and credit memo data in the default Magento store. Hence, Tridhyatech has created the Delete Order Extension to allow the backend user to do such operations.

The Delete Order extension allows the store owners to define the allowed order statuses in the system configurations. Based on the allowed order status, the store admin can delete the orders, shipments, invoices, and credit memos from the respective sales grid or details page. By this extension, the store owners can delete multiple invoices, shipments, credit memos from the grid. Also, the store admin can delete invoices, shipments, credit memos, and orders from the details page as well.

## Key Features:

- Multi-store support.
- Ability to delete the order from the sales order grid & order view page.
- Ability to delete only those orders which status select in system config.
- Ability to delete multiple invoices from the sales order invoice grid & order view invoice grid.
- Ability to delete invoices from the invoice view page.
- Ability to delete multiple credit memos from the sales order credit memo grid & order view credit memo grid.
- Ability to delete credit memo from the credit memo view page.
- Ability to delete multiple shipments from the sales order shipment grid & order view shipment grid.
- Ability to delete shipment from the shipment view page.
- Ability to change order status after delete invoice or credit memo or shipment.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/deleteorder
        * php bin/magento module:enable Tridhyatech_DeleteOrder
        * php bin/magento setup:upgrade