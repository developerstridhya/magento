<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_DeleteOrder
 */
namespace Tridhyatech\DeleteOrder\Ui\Component\Control;

use Magento\Ui\Component\Control\Action;

class Delete extends Action
{
    /**
     * @inheritdoc
     */
    public function prepare()
    {
        $config = $this->getConfiguration();
        $context = $this->getContext();
        $config['url'] = $context->getUrl(
            $config['delete'],
            ['order_id' => $context->getRequestParam('order_id')]
        );
        $this->setData('config', (array)$config);
        parent::prepare();
    }
}
