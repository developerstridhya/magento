<?php

namespace Tridhyatech\DeleteOrder\Plugin;

class BtnCreditmemoView
{
    /**
     * @var helper
     */
    protected $helper;

    /**
     * @param \Tridhyatech\DeleteOrder\Helper\Data $helper
     */
    public function __construct(
        \Tridhyatech\DeleteOrder\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Add delete button
     *
     * @param \Magento\Sales\Block\Adminhtml\Order\Creditmemo\View $subject
     * @return null
     */
    public function beforeSetLayout(\Magento\Sales\Block\Adminhtml\Order\Creditmemo\View $subject)
    {
        $params = $subject->getRequest()->getParams();
        $enable = $this->helper->getConfig('deleteorder/general/enable');
        if ($enable == 1) {
            $message = __('Are you sure you want to do this?');
            if ($subject->getRequest()->getFullActionName() == 'sales_order_creditmemo_view') {
                $subject->addButton(
                    'creditmemo_delete',
                    [
                        'label' => __('Delete'),
                        'onclick' => "confirmSetLocation(
                                     '{$message}', 
                                     '{$subject->getUrl('deleteorder/creditmemo/deleteButton', ['creditmemo_id' => $params['creditmemo_id']])}')",
                        'class' => 'delete'
                    ]
                );
            }
        }

        return null;
    }
}
