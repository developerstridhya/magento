<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_DeleteOrder
 */
namespace Tridhyatech\DeleteOrder\Model\Shipping;

use Magento\Framework\App\ResourceConnection;

class DeleteShipping
{
    /**
     * Shipment Grid Table
     */
    protected const SHIPMENT_GRID_TABLE = 'sales_shipment_grid';

    /**
     * Shipment Table
     */
    protected const SHIPMENT_TABLE = 'sales_shipment';

    /**
     * @var resourceConnection
     */
    protected $resourceConnection;
    /**
     * @var shipment
     */
    protected $shipment;
    /**
     * @var order
     */
    protected $order;

    /**
     * @param \Magento\Sales\Model\Order\Shipment $shipment
     * @param \Magento\Sales\Model\Order $order
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Sales\Model\Order\Shipment $shipment,
        \Magento\Sales\Model\Order $order,
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->shipment = $shipment;
        $this->order = $order;
    }

     /**
      * Delete shipment
      *
      * @param int $shipmentId
      * @return order
      */
    public function delete($shipmentId)
    {
        $connection  = $this->resourceConnection->getConnection();
        $shipmentGridTable = $connection->getTableName(self::SHIPMENT_GRID_TABLE);
        $shipmentTable = $connection->getTableName(self::SHIPMENT_TABLE);

        $shipment = $this->shipment->load($shipmentId);
        $orderId = $shipment->getOrder()->getId();
        $order = $this->order->load($orderId);

        foreach ($order->getAllItems() as $orderItem) {
            foreach ($shipment->getAllItems() as $shipmentItem) {
                if ($shipmentItem->getOrderItemId() == $orderItem->getItemId()) {
                    $orderItem->setQtyShipped($orderItem->getQtyShipped() - $shipmentItem->getQty());
                }
            }
        }

        $whereConditions = [
            $connection->quoteInto('entity_id = ?', $shipmentId),
        ];
        $connection->delete($shipmentGridTable, $whereConditions);
        $connection->delete($shipmentTable, $whereConditions);

        if ($order->hasShipments() || $order->hasInvoices() || $order->hasCreditmemos()) {
            $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING)
                ->setStatus($order->getConfig()->getStateDefaultStatus(\Magento\Sales\Model\Order::STATE_PROCESSING))
                ->save();
        } else {
            $order->setState(\Magento\Sales\Model\Order::STATE_NEW)
                ->setStatus($order->getConfig()->getStateDefaultStatus(\Magento\Sales\Model\Order::STATE_NEW))
                ->save();
        }

        return $order;
    }
}
