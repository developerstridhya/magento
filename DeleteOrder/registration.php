<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_DeleteOrder
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Tridhyatech_DeleteOrder',
    __DIR__
);
