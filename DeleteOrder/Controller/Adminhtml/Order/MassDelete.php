<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_DeleteOrder
 */
namespace Tridhyatech\DeleteOrder\Controller\Adminhtml\Order;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\App\ResourceConnection;

class MassDelete extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction implements HttpPostActionInterface
{
    protected const INVOICE_GRID_TABLE = 'sales_invoice_grid';
    protected const SHIPMENT_GRID_TABLE = 'sales_shipment_grid';
    protected const CREDITMEMO_GRID_TABLE = 'sales_creditmemo_grid';

    /**
     * @var logger
     */
    protected $logger;
    /**
     * @var helper
     */
    protected $helper;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Tridhyatech\DeleteOrder\Helper\Data $helper
     * @param ResourceConnection $resourceConnection
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Tridhyatech\DeleteOrder\Helper\Data $helper,
        ResourceConnection $resourceConnection,
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($context, $filter);
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->helper = $helper;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    protected function massAction(AbstractCollection $collection)
    {
        $array = $this->helper->getConfig('deleteorder/general/order_status');
        $status = explode(',', $array);
        $countDeleteOrder = 0;
        $id = [];
        foreach ($collection->getItems() as $order) {
            if (in_array($order->getStatus(), $status)) {
                $order->setId($order->getEntityId());
                $order->delete();
                $countDeleteOrder++;

                $connection  = $this->resourceConnection->getConnection();
                $invoiceGridTable = $connection->getTableName(self::INVOICE_GRID_TABLE);
                $shippmentGridTable = $connection->getTableName(self::SHIPMENT_GRID_TABLE);
                $creditmemoGridTable = $connection->getTableName(self::CREDITMEMO_GRID_TABLE);
                $whereConditions = [
                    $connection->quoteInto('order_id = ?', $order->getEntityId()),
                ];
                $connection->delete($invoiceGridTable, $whereConditions);
                $connection->delete($shippmentGridTable, $whereConditions);
                $connection->delete($creditmemoGridTable, $whereConditions);
            } else {
                array_push($id, $order->getIncrementId());
            }
        }

        if ($id) {
            $error_id = implode(", #", $id);
            $error_name = "Order(s) #".$error_id." can not be deleted. Please check system configuration for the allowed status to delete order.";
            $this->messageManager->addError(__($error_name));
        }

        if ($countDeleteOrder) {
            $this->messageManager->addSuccessMessage(__('A total of %1 order(s) were deleted.', $countDeleteOrder));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->getComponentRefererUrl());
        return $resultRedirect;
    }
}
