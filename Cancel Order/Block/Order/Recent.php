<?php

/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_CancelOrder
 */

namespace Tridhyatech\CancelOrder\Block\Order;

/**
 * Recent order history block
 */
class Recent extends \Magento\Sales\Block\Order\Recent
{
    /**
     * Return order-cancellation URL
     *
     * @param Object $order
     *
     * @return string
     */
    public function getFormAction($order)
    {
        return $this->getUrl('cancelOrder/order/cancel', ['order_id' => $order->getId(), '_secure' => true]);
    }
}
