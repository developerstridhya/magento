# Cancel Order By Tridhya Tech

## Description

Why is the ability to cancel an order important for the Magento store Owner? Sometimes, the customer may not be 1000% certain of the purchase after placing an order on your store. There may be many reasons they need to cancel the order like they have found that same product at a lower price somewhere else. They also may not be sure about the product or they have personal reasons like an address change. In these types of situations, the Tridhyatech Cancel Order extension provides your customers' facility to cancel their order and build trust among your customers.

Cancel Order by Tridhyatech allows a customer to cancel their orders from the frontend. With Tridhyatech Cancel Order extension, your website has these advantages:

    1. Customers can cancel their order from the frontend.
    2. This order will be notified to admin and customer via an Email with order cancellation details.
    3. The admin can view the list of all canceled orders in the backend grid along with the customer specified reason and additional comments.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/cancelorder
        * php bin/magento module:enable Tridhyatech_CancelOrder
        * php bin/magento setup:upgrade
