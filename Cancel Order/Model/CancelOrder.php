<?php

/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_CancelOrder
 */

namespace Tridhyatech\CancelOrder\Model;

class CancelOrder extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Model construct that should be used for object initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Tridhyatech\CancelOrder\Model\ResourceModel\CancelOrder::class);
    }
}
