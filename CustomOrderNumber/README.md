# Custom Order Number By Tridhya Tech

The Custom Order Number extension allows the store owner to modify the order, invoice, shipment, and credit memo numbers by utilizing distinct values such as Prefix. This amazing extension allows the store owner to easily set up the order numbering of their online store. It is capable of providing the store owners to customize numbers of the order, invoice, shipment, and credit memo IDs according to their needs. It also gives an option to use the format of the order number for the invoice, shipment, and credit memo itself.

## Key Features:

- Ability to Enable/Disable Extension.
- Ability to Enable/Disable for individual configuration for numbers like order, invoice, credit memo.
- Ability to customized order, invoice, shipment, and memo numbers in the Magento 2 store
- Ability to add unique format for order, invoice, shipment number as you want.
- Ability to set number start, increments step, and number padding for ids.
- Ability to Reset counter configure to reset daily, weekly, monthly, yearly.
- Ability to replace a specific order number part with a prefix for invoice, shipment, credit memo.
- Compatible up to 2.4.x.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/customordernumber
        * php bin/magento module:enable Tridhyatech_CustomOrderNumber
        * php bin/magento setup:upgrade