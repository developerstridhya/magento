<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_CustomOrderNumber
 */
namespace Tridhyatech\CustomOrderNumber\Model\Config\Source;

class CronOption implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Constant cron never
     */
    const CRON_NEVER = '0';

    /**
     * Constant cron everyday
     */
    const CRON_EVERYDAY = '1';

    /**
     * Constant cron everyweek
     */
    const CRON_EVERYWEEK = '2';

    /**
     * Constant cron every month
     */
    const CRON_EVERYMONTH = '3';

    /**
     * Constant cron every year
     */
    const CRON_EVERYYEAR = '4';

    /**
     * Returns options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::CRON_NEVER, 'label' => __('Never')],
            ['value' => self::CRON_EVERYDAY, 'label' => __('Every Day')],
            ['value' => self::CRON_EVERYWEEK, 'label' => __('Every Week')],
            ['value' => self::CRON_EVERYMONTH, 'label' => __('Every Month')],
            ['value' => self::CRON_EVERYYEAR, 'label' => __('Every Year')],
        ];
    }
}
