<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_CustomOrderNumber
 */
namespace Tridhyatech\CustomOrderNumber\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OrderNumberObserver implements ObserverInterface
{
    /**
     * @var \Tridhyatech\CustomOrderNumber\Helper\Data
     */
    protected $helperData;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    
    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $session;

    /**
     * @var \Tridhyatech\CustomOrderNumber\Model\ResourceModel\NumberSequence
     */
    protected $numberSequence;

    /**
     * Construct Method
     *
     * @param \Tridhyatech\CustomOrderNumber\Helper\Data $helperData
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Backend\Model\Session\Quote $session
     * @param \Tridhyatech\CustomOrderNumber\Model\ResourceModel\NumberSequence $numberSequence
     */
    public function __construct(
        \Tridhyatech\CustomOrderNumber\Helper\Data $helperData,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Backend\Model\Session\Quote $session,
        \Tridhyatech\CustomOrderNumber\Model\ResourceModel\NumberSequence $numberSequence
    ) {
            $this->helperData = $helperData;
            $this->storeManager = $storeManager;
            $this->session = $session;
            $this->numberSequence = $numberSequence;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        if ($this->helperData->moduleEnable() && $this->helperData->orderEnable()) {
            $storeId = $this->storeManager->getStore()->getStoreId();
            $sessionId = $this->session->getStoreId();
            if (isset($sessionId)) {
                $storeId = $sessionId;
            }
             
            $numberFormat = $this->helperData->orderNumberFormat($storeId);
            $startCounterValue = $this->helperData->startOrderCounter($storeId);
            $incrementStep = $this->helperData->orderIncrementStep($storeId);
            $paddingNumber = $this->helperData->orderNumberPadding($storeId);
            $pattern = "%0".$paddingNumber."d";
            $entityType = 'order';
            if ($this->helperData->enableIndividualOrder($storeId)) {
                if ($storeId == 1) {
                    $table = $this->numberSequence->getSequenceNumberTable($entityType, '0');
                } else {
                    $table = $this->numberSequence->getSequenceNumberTable($entityType, $storeId);
                }
            } else {
                $table = $this->numberSequence->getSequenceNumberTable($entityType, '0');
            }
            $counter = $this->numberSequence->getCounter($table, $startCounterValue, $incrementStep, $pattern);
            $result = $this->numberSequence->getReplaceNum($numberFormat, $storeId, $counter);
            $unique = $this->numberSequence->isUniqueNum($result);
            if ($unique == 0) {
                $i = 1;
                $check = $result;
                do {
                    $unique = $this->numberSequence->isUniqueNum($check);
                    if ($unique == '0') {
                        $check = $result.'-'.$i;
                        $i++;
                    }
                    if ($unique == '1') {
                        $result = $check;
                    }
                } while ($unique == '0');
            }
            $getOrder = $observer->getOrder();
            $getOrder->setIncrementId($result);
        }
    }
}
