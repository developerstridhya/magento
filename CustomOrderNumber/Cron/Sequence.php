<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_CustomOrderNumber
 */
namespace Tridhyatech\CustomOrderNumber\Cron;

use Tridhyatech\CustomOrderNumber\Model\Config\Source\CronOption;

class Sequence
{
    /**
     * @var \Tridhyatech\CustomOrderNumber\Model\ResourceModel\NumberSequence
     */
    protected $numberSequence;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Construct Method
     *
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Tridhyatech\CustomOrderNumber\Model\ResourceModel\NumberSequence $numberSequence
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Tridhyatech\CustomOrderNumber\Model\ResourceModel\NumberSequence $numberSequence
    ) {
        $this->storeManager = $storeManager;
        $this->numberSequence = $numberSequence;
    }

    /**
     * returns cron every day value
     *
     * @return mixed
     */
    public function cronEveryDay()
    {
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $storeId = $store->getStoreId();
            $this->numberSequence->setCron($storeId, CronOption::CRON_EVERYDAY);
        }
        return $this;
    }

    /**
     * returns cron every week value
     *
     * @return mixed
     */
    public function cronEveryWeek()
    {
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $storeId = $store->getStoreId();
            $this->numberSequence->setCron($storeId, CronOption::CRON_EVERYWEEK);
        }
        return $this;
    }

    /**
     * returns cron every month value
     *
     * @return mixed
     */
    public function cronEveryMonth()
    {
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $storeId = $store->getStoreId();
            $this->numberSequence->setCron($storeId, CronOption::CRON_EVERYMONTH);
        }
        return $this;
    }

    /**
     * returns cron every year value
     *
     * @return mixed
     */
    public function cronEveryYear()
    {
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $storeId = $store->getStoreId();
            $this->numberSequence->setCron($storeId, CronOption::CRON_EVERYYEAR);
        }
        return $this;
    }
}
