<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_CustomOrderNumber
 */
namespace Tridhyatech\CustomOrderNumber\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_EMAILCART = 'customordernumber/';
    protected $context;

    public function __construct(Context $context)
    {
        $this->context = $context;
        parent::__construct($context);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function timezone($storeId = null)
    {
        return $this->scopeConfig->getValue(
            'general/locale/timezone',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function moduleEnable($storeId = null)
    {
        return $this->getConfigValue('customordernumber/module/enable', $storeId);
    }

    public function orderEnable($storeId = null)
    {
        return $this->getConfigValue('customordernumber/general/enable', $storeId);
    }

    public function orderNumberFormat($storeId = null)
    {
        return $this->getConfigValue('customordernumber/general/number_format', $storeId);
    }

    public function startOrderCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/general/start_counter', $storeId);
    }

    public function orderIncrementStep($storeId = null)
    {
        return $this->getConfigValue('customordernumber/general/increment_counter', $storeId);
    }

    public function orderNumberPadding($storeId = null)
    {
        return $this->getConfigValue('customordernumber/general/padding_counter', $storeId);
    }

    public function resetOrderCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/general/reset_counter', $storeId);
    }

    public function enableIndividualOrder($storeId = null)
    {
        return $this->getConfigValue('customordernumber/general/individual', $storeId);
    }

    public function invoiceEnable($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/enable', $storeId);
    }

    public function invoiceSameAsOrder($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/same_as_order', $storeId);
    }

    public function invoiceNumberFormat($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/number_format', $storeId);
    }

    public function startInvoiceCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/start_counter', $storeId);
    }

    public function invoiceIncrementStep($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/increment_counter', $storeId);
    }

    public function invoiceNumberPadding($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/padding_counter', $storeId);
    }

    public function enableIndividualInvoice($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/individual', $storeId);
    }

    public function resetInvoiceCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/reset_counter', $storeId);
    }

    public function invoiceReplaceTo($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/replace_to', $storeId);
    }

    public function invoiceReplaceWith($storeId = null)
    {
        return $this->getConfigValue('customordernumber/invoice_configuration/replace_with', $storeId);
    }

    public function shipmentEnable($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/enable', $storeId);
    }

    public function shipmentSameAsOrder($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/same_as_order', $storeId);
    }

    public function shipmentNumberFormat($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/number_format', $storeId);
    }

    public function startShipmentCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/start_counter', $storeId);
    }

    public function shipmentIncrementStep($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/increment_counter', $storeId);
    }

    public function shipmentNumberPadding($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/padding_counter', $storeId);
    }

    public function enableIndividualShipment($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/individual', $storeId);
    }

    public function resetShipmentCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/reset_counter', $storeId);
    }

    public function shipmentReplaceTo($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/replace_to', $storeId);
    }

    public function shipmentReplaceWith($storeId = null)
    {
        return $this->getConfigValue('customordernumber/shipment_configuration/replace_with', $storeId);
    }

    public function creditmemoEnable($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/enable', $storeId);
    }

    public function creditmemoSameAsOrder($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/same_as_order', $storeId);
    }

    public function creditmemoNumberFormat($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/number_format', $storeId);
    }

    public function startCreditmemoCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/start_counter', $storeId);
    }

    public function creditmemoIncrementStep($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/increment_counter', $storeId);
    }

    public function creditmemoNumberPadding($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/padding_counter', $storeId);
    }

    public function enableIndividualCreditmemo($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/individual', $storeId);
    }

    public function resetCreditmemoCounter($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/reset_counter', $storeId);
    }

    public function creditmemoReplaceTo($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/replace_to', $storeId);
    }

    public function creditmemoReplaceWith($storeId = null)
    {
        return $this->getConfigValue('customordernumber/creditmemo_configuration/replace_with', $storeId);
    }
}
