<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhyatech.com)
 * @package Tridhyatech_CustomOrderNumber
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Tridhyatech_CustomOrderNumber',
    __DIR__
);
