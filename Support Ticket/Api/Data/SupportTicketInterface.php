<?php

/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_SupportTicket
 */

namespace Tridhyatech\SupportTicket\Api\Data;

interface SupportTicketInterface
{
    public const ENTITY_ID = 'entity_id';
    public const NAME = 'name';
    public const EMAIL = 'email';
    public const CUSTOMER_ID = 'customer_id';
    public const SUBJECT = 'subject';
    public const DEPARTMENT_ID = 'department_id';
    public const STATUS_ID = 'status_id';
    public const PRIORITY_ID = 'priority_id';
    public const ORDER = 'order_data';
    public const CREATED_DATE = 'created_date';
    public const UPDATED_DATE = 'updated_date';
    public const EMAIL_SEND = 'email_send';

    /**
     * Get Entity Id
     *
     * @return int
     */
    public function getEntityId();

    /**
     * Set Entity Id
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Get Name
     *
     * @return string
     */
    public function getName();

    /**
     * Set Name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get Email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set Email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email);

    /**
     * Get Customer Id
     *
     * @return int
     */
    public function getCustomerId();

    /**
     * Set Customer Id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Get Subject
     *
     * @return string
     */
    public function getSubject();

    /**
     * Set Subject
     *
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject);

    /**
     * Get Dept Id
     *
     * @return int
     */
    public function getDepartmentId();

    /**
     * Set Dept Id
     *
     * @param int $departmentId
     * @return $this
     */
    public function setDepartmentId($departmentId);

    /**
     * Get Status Id
     *
     * @return int
     */
    public function getStatusId();

    /**
     * Set Status Id
     *
     * @param int $statusId
     * @return $this
     */
    public function setStatusId($statusId);

    /**
     * Get Priority Id
     *
     * @return int
     */
    public function getPriorityId();

    /**
     * Set Priority Id
     *
     * @param int $priorityId
     * @return $this
     */
    public function setPriorityId($priorityId);

    /**
     * Get Order
     *
     * @return string
     */
    public function getOrder();

    /**
     * Set Order
     *
     * @param string $order
     * @return $this
     */
    public function setOrder($order);

    /**
     * Get Created date
     *
     * @return string
     */
    public function getCreatedDate();

    /**
     * Set Created Date
     *
     * @param string $createdDate
     * @return $this
     */
    public function setCreatedDate($createdDate);

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedDate();

    /**
     * Set updated Date
     *
     * @param string $updatedDate
     * @return $this
     */
    public function setUpdatedDate($updatedDate);

    /**
     * Get Email Send
     *
     * @return int
     */
    public function getEmailSend();

    /**
     * Set Email Send
     *
     * @param int $emailSend
     * @return $this
     */
    public function setEmailSend($emailSend);
}
