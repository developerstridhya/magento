# Support Ticket By Tridhya Tech

## Description

The Support Ticket extension allows the customers to open the support tickets regarding their queries related to the store and thus communicate with the store owners related to those queries. The customers can see their past and current tickets under the tab in the My Account pages. The store admin can add status, priority, and department categories from the backend. And based on that categories, customers can add support requests. After successful conversation/support, the customers can close the open tickets. The emails will also be sent out to the customers when the store admin replies/closes the support ticket. And emails will be sent out to the store admin when the customer generates a new support ticket.

## Installation

Please use composer to install the extension.

    1. At your Magento root:

        * composer require tridhyatech/supportticket
        * php bin/magento module:enable Tridhyatech_SupportTicket
        * php bin/magento setup:upgrade
