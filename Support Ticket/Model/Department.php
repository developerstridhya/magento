<?php

/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_SupportTicket
 */

namespace Tridhyatech\SupportTicket\Model;

use Tridhyatech\SupportTicket\Api\Data\TicketDepartmentInterface;

class Department extends \Magento\Framework\Model\AbstractModel implements TicketDepartmentInterface
{
    public const CACHE_TAG = 'tridhyatech_support_ticket_department';

    /**
     * @var string
     */
    protected $_cacheTag = 'tridhyatech_support_ticket_department';

    /**
     * @var string
     */
    protected $_eventPrefix = 'tridhyatech_support_ticket_department';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(\Tridhyatech\SupportTicket\Model\ResourceModel\Department::class);
    }

    /**
     * @inheritDoc
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedDate()
    {
        return $this->getData(self::CREATED_DATE);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedDate($createdDate)
    {
        return $this->setData(self::CREATED_DATE, $createdDate);
    }
}
