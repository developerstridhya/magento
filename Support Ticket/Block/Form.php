<?php

/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2023 Tridhyatech (https://www.tridhyatech.com/)
 * @package Tridhyatech_SupportTicket
 */

namespace Tridhyatech\SupportTicket\Block;

class Form extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Tridhyatech\SupportTicket\Model\PriorityFactory
     */
    protected $priorityFactory;

    /**
     * @var \Tridhyatech\SupportTicket\Model\DepartmentFactory
     */
    protected $departmentFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $order;

    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    protected $customerSession;

    /**
     * Construct Method
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Tridhyatech\SupportTicket\Model\PriorityFactory $priorityFactory
     * @param \Tridhyatech\SupportTicket\Model\DepartmentFactory $departmentFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Customer\Model\SessionFactory $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Tridhyatech\SupportTicket\Model\PriorityFactory $priorityFactory,
        \Tridhyatech\SupportTicket\Model\DepartmentFactory $departmentFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\SessionFactory $customerSession,
        array $data = []
    ) {
        $this->priorityFactory = $priorityFactory;
        $this->departmentFactory = $departmentFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * Get Priority Options
     *
     * @return string
     */
    public function getPriorityOptions()
    {
        $priorityCollection = $this->priorityFactory->create()->getCollection();
        $options = [];
        foreach ($priorityCollection as $item) {
            $html = '<option value="' . $item->getEntityId() . '">' . $item->getTitle() . '</option>';
            array_push($options, $html);
        }
        $htmlData = implode(" ", $options);
        return $htmlData;
    }

    /**
     * Get Dept Options
     *
     * @return string
     */
    public function getDepartmentOptions()
    {
        $departmentCollection = $this->departmentFactory->create()->getCollection();
        $options = [];
        foreach ($departmentCollection as $item) {
            $html = '<option value="' . $item->getEntityId() . '">' . $item->getTitle() . '</option>';
            array_push($options, $html);
        }
        $htmlData = implode(" ", $options);
        return $htmlData;
    }

    /**
     * Get Order Ids
     *
     * @return string
     */
    public function getOrderIds()
    {
        $cid = $this->customerSession->create()->getCustomer()->getId();
        $orderCollection = $this->orderCollectionFactory->create()->addFieldToFilter('customer_id', $cid);
        $options = [];
        foreach ($orderCollection as $order) {
            $html = '<option value="' . $order->getIncrementId() . '">' . $order->getIncrementId() . '</option>';
            array_push($options, $html);
        }
        $htmlData = implode(" ", $options);
        return $htmlData;
    }
}
